function fetchPdv() {
    $.ajax({
        url: "/endpoints/get-contingencia",
        type: "get",
        data: { city: $("#cities_covid").val(), takeout: $("#takeout").is(":checked"), rappi: $("#rappi").is(":checked"), own_delivery: $("#own_delivery").is(":checked"), reopening: $("#reopening").is(":checked") },
        success: function (e) {
            $("#pos_table_content").empty(),
                jQuery.each(e.pdv, function (e, t) {
                    $("#pos_table_content").append("<tr id=" + e + "></tr>"),
                        $("#" + e).append(
                            "<td data-label='Restaurante'>" +
                                t.pdv +
                                "</td><td data-label='Dirección'>" +
                                t.address +
                                ", " +
                                t.urb +
                                "</td><td data-label='Ciudad'>" +
                                t.city +
                                "</td><td data-label='Pasa, pide y lleva'>" +
                                (t.pick_able ? "SI" : "NO") +
                                "</td><td data-label='Programa tu pedido y recoge'>" +
                                (t.rappi ? "SI" : "NO") +
                                "</td><td data-label='Domicilio en línea o llámanos'>" +
                                (t.own_delivery ? "SI" : "NO") +
                                "</td><td data-label='Comedor Abierto'>" +
                                (t.dinning_room ? "Plazoleta" : "Comedor") +
                                "<br>" +
                                (t.reopening ? "Abiert" : "Cerrad") +
                                (t.dinning_room ? "a" : "o") +
                                "&nbsp;" +
                                (t.reopening && t.open_time ? t.open_time : "") +
                                "</td><td data-label='Horario restaurante'>" +
                                t.pick_time +
                                "</td>"
                        );
                });
        },
    }).fail(function (e, t, a, n) {
        console.log(a);
    });
}
$("#cities_covid").change(function () {
    fetchPdv();
}),
    $("#takeout").change(function () {
        fetchPdv();
    }),
    $("#rappi").change(function () {
        fetchPdv();
    }),
    $("#own_delivery").change(function () {
        fetchPdv();
    }),
    $("#reopening").change(function () {
        fetchPdv();
    });
