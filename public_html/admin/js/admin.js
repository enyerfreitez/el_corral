count = 0;

$("#adder").click(function(){
    adder()
})
$(".remover").click(function(){
    
})

function adder(){
    count = count + 1;
    if($("#type").val() == 1){
        $("#content_wrapper").append(
            '<div class="form-group"> ' +
                '<label for="redirection_url"> Video de interna '+count+'</label>' +
                '<p>Añadir acá el enlace al video que quieres que aparezca. SOLO VIDEOS YOUTUBE</p>' +
                '<input type="text" name="content['+count+']">' +
                '<button type="button" onclick="remover('+count+');" id="'+count+'">' +
                    'Eliminar' +
                '</button>' +
            '</div>'
        );
    }else{
        $("#content_wrapper").append(
            '<div class="form-group"> ' +
                '<label for="redirection_url"> Imagen de interna '+count+'</label>' +
                '<p>Recuerda que las dimensiones del banner para el new deben ser de 1982 x 669px.</p>' +
                '<input type="file" name="content['+count+']">' +
                '<button type="button" onclick="remover('+count+');" id="'+count+'">' +
                    'Eliminar' +
                '</button>' +
            '</div>'
        );
    }
}

function remover(id){
    $("#"+id).closest(".form-group").remove()
}

$( ".row_position" ).sortable({
    
    delay: 150,
    stop: function() {
        var selectedData = new Array();
        var entity = $(this).attr("id");
        console.log(entity);
        $('#'+entity+'>tr').each(function() {
            selectedData.push($(this).attr("id"));
        });
        updateOrder(selectedData, entity);
    }
});


function updateOrder(data, e) {
    $.ajax({
        url:"/adm/sort",
        type:'get',
        data:{
            position : data,
            entity : e
        },
        success:function(){
            $.notify({
                title: '<strong>¡Muy Bien!</strong>',
                message: 'Reorganizado con exito.'
            },{
                type: 'success',
                timer: 10000
            });
        }
    })
}