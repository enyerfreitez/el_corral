<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ProductsCategorySeeder::class);
        $this->call(SlideSeeder::class);
        $this->call(RestaurantsTableSeeder::class);
        $this->call(BannerRestaurantSeeder::class);
    }
}
