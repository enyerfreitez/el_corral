<?php

use Illuminate\Database\Seeder;

class SlideSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $slides = array(
            array(
                'title' => "¡Deliciosos vaqueros a la parrilla!",
                //'copy' => "¿Ya probaste nuestro Vaquero Mexicano?",
                'uri' => "/lo-nuevo/vaqueros",
                'img_uri' => 'img/sliders/Vaqueros.jpg',
                'order' => 1
            ),
            array(
                'title' => "¡Nuevo Sándwich de Pollo, hecho con 100% pechuga!",
                //'copy' => "¿Ya probaste nuestro Vaquero Mexicano?",
                'uri' => "/lo-nuevo/sandwich-pollo-apanado",
                'img_uri' => 'img/sliders/Pollo-Apanado.jpg',
                'order' => 1
            ),
            array(
                'title' => "¡Imposible resistirse a una Todoterreno!",
                //'copy' => "Hecha a la parrilla, con los mejores ingredientes",
                'uri' => "/lo-nuevo/todoterreno",
                'img_uri' => 'img/sliders/TodoTerreno.jpg',
                'order' => 2
            ),
            array(
                'title' => "¡Vuelve el Festival de Malteadas!",
                //'copy' => "Recargadas con los toppings más deliciosos",
                'uri' => "/lo-nuevo/malteadas",
                'img_uri' => 'img/sliders/Malteadas.jpg',
                'order' => 3
            ),
            array(
                'title' => "¡Tus sabores favoritos en un cono!",
                //'copy' => "Pidelo en galleta o en vaso",
                'uri' => "/lo-nuevo/conos",
                'img_uri' => 'img/sliders/Conos.jpg',
                'order' => 4
            ),
            array(
                'title' => "Pa' Tardear, ¡De lunes a viernes, de 2 a 6 PM!",
                //'copy' => "De lunes a viernes - 2 pm a 6 pm",
                'uri' => null,
                'img_uri' => 'img/sliders/Pa-Tardear.jpg',
                'order' => 5
            ),
            array(
                'title' => "¿Ya probaste nuestros deliciosos desayunos?",
                //'copy' => "En pan, wrap o arepa, con jugo de naranja o café",
                'uri' => "/lo-nuevo/desayunos",
                'img_uri' => 'img/sliders/Desayunos.jpg',
                'order' => 6
            ),
            array(
                'title' => "¡Corralazos: Doble Doble, Callejera y Pollo Mexicano!",
                //'copy' => "Con papas medianas y gaseosa de 16 Oz",
                'uri' => "/lo-nuevo/corralazos",
                'img_uri' => 'img/sliders/Corralazos.jpg',
                'order' => 7
            ),
            array(
                'title' => "¡Realiza tu evento con nosotros!",
                //'copy' => "Contáctanos al 018000114722",
                'uri' => "/eventos-fiestas-y-bonos/foodtruck-y-eventos",
                'img_uri' => 'img/sliders/FT.jpg',
                'order' => 8
            ),
            array(
                'title' => "¡Conoce nuestra nueva imagen!",
                //'copy' => "Contáctanos al 018000114722",
                'uri' => null,
                'img_uri' => 'img/sliders/Remodelaciones.jpg',
                'order' => 9
            ),
        );
        DB::table('slides')->insert($slides);
    }
}
