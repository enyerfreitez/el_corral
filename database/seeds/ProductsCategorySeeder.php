<?php

use Illuminate\Database\Seeder;
use App\ProductCategory;

class ProductsCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        /** 1  Hamburguesas
         *
         */
            $pc = ProductCategory::create([
                'name' => "Hamburguesas",
                'order' => 1,
                'img_uri' => "/img/our-cart/hamburguesa.jpg",
                'slug' => "hamburguesas"
            ]);

            $products = array(
                array(
                    'name' => "Corral",
                    'subcategory' => "Tradicional",
                    'description' => 'Jugosa carne 100% de res de 125 gramos, con verduras y salsas.',
                    'img_uri' => '/img/our-cart/hamburguesas/corral.jpg',
                    'pcategory_id' => $pc->id,
                    'order' => 1
                ),
                array(
                    'name' => "Corral Queso",
                    'subcategory' => "Tradicional",
                    'description' => 'Jugosa carne 100% de res de 125 gramos, con queso mozzarella delicioso, verduras y salsas.',
                    'img_uri' => '/img/our-cart/hamburguesas/queso.jpg',
                    'pcategory_id' => $pc->id,
                    'order' => 1
                ),
                array(
                    'name' => "Corralita",
                    'subcategory' => "Tradicional",
                    'description' => 'Jugosa carne 100% de res de 90 gramos, verduras y salsas.',
                    'img_uri' => '/img/our-cart/hamburguesas/corralita.jpg',
                    'pcategory_id' => $pc->id,
                    'order' => 1
                ),
                array(
                    'name' => "Dualisíma",
                    'subcategory' => "Tradicional",
                    'description' => 'Dos jugosas carnes 100% de res de 90 gramos cada una, doble queso, pepinillos y esponjoso pan brioche.',
                    'img_uri' => '/img/our-cart/hamburguesas/dualisima.jpg',
                    'pcategory_id' => $pc->id,
                    'order' => 1
                ),
                array(
                    'name' => "Corral Callejera",
                    'subcategory' => "Corralazos",
                    'description' => 'Jugosa carne 100% de res de 125 gramos, queso mozzarella, papas chip, salsas y pan tipo brioche.',
                    'img_uri' => '/img/our-cart/hamburguesas/callejera.jpg',
                    'pcategory_id' => $pc->id,
                    'order' => 1
                ),
                array(
                    'name' => "Doble Doble",
                    'subcategory' => "Corralazos",
                    'description' => 'Dos jugosas carnes 100% de res de 90 gramos cada una, dos tajadas de queso mozzarella, cebolla caramelizada, lechuga fresca, tomate fresco, mayonesa y pan tipo brioche.',
                    'img_uri' => '/img/our-cart/hamburguesas/doble-doble-vertical.jpg',
                    'pcategory_id' => $pc->id,
                    'order' => 1
                ),
                array(
                    'name' => "Corral Costeña",
                    'subcategory' => "Especialidades",
                    'description' => 'Jugosa carne 100% de res de 125 gramos, chips de plátano, espectacular queso costeño, suero y mayonesa.',
                    'img_uri' => '/img/our-cart/hamburguesas/costeña.jpg',
                    'pcategory_id' => $pc->id,
                    'order' => 1
                ),
                array(
                    'name' => "Corral Hawaiana",
                    'subcategory' => "Especialidades",
                    'description' => 'Jugosa carne 100% de res de 125 gramos, queso mozzarella, dulce piña, lechuga fresca y salsas.',
                    'img_uri' => '/img/our-cart/hamburguesas/hawaiana.jpg',
                    'pcategory_id' => $pc->id,
                    'order' => 1
                ),
                array(
                    'name' => "Philadelphia Steak",
                    'subcategory' => "Especialidades",
                    'description' => 'Tierno lomo de res a la plancha de 154 gramos, con cebolla grillé, delicioso queso americano, mayonesa y pimienta negra.',
                    'img_uri' => '/img/our-cart/hamburguesas/philadelphia-steak.jpg',
                    'pcategory_id' => $pc->id,
                    'order' => 1
                ),
                array(
                    'name' => "Corral Criolla",
                    'subcategory' => "Especialidades",
                    'description' => 'Jugosa carne 100% de res de 125 gramos, queso mozzarella, huevo frito, cebolla grillé, tomate, fresca lechuga y salsas.',
                    'img_uri' => '/img/our-cart/hamburguesas/criolla.jpg',
                    'pcategory_id' => $pc->id,
                    'order' => 1
                ),
                array(
                    'name' => "Corral Mexicana",
                    'subcategory' => "Especialidades",
                    'description' => 'Jugosa carne 100% de res de 125 gramos, con queso americano, increíble guacamole, fríjol refrito, verduras y mayonesa.',
                    'img_uri' => '/img/our-cart/hamburguesas/corral-mexicana.jpg',
                    'pcategory_id' => $pc->id,
                    'order' => 1
                ),
                array(
                    'name' => "Todoterreno",
                    'subcategory' => "A la parrilla",
                    'description' => 'Doble carne 100% de res de 1/2 libra a la parrilla, con tocineta, queso mozzarella, verduras, pepinillos y salsa BBQ.',
                    'img_uri' => '/img/our-cart/hamburguesas/todoterreno.jpg',
                    'pcategory_id' => $pc->id,
                    'order' => 1
                ),
                array(
                    'name' => "Corralísima 1/2 Libra",
                    'subcategory' => "A la Parrilla",
                    'description' => 'Jugosa carne 100% de res de 1/2 libra asada a la parrilla, queso mozzarella, con deliciosa salsa BBQ y verduras.',
                    'img_uri' => '/img/our-cart/hamburguesas/1-2-libra.jpg',
                    'pcategory_id' => $pc->id,
                    'order' => 1
                ),
                array(
                    'name' => "Corralísima 3/4 Libra",
                    'subcategory' => "A la parrilla",
                    'description' => 'Jugosa carne 100% de res de 3/4 de libra asada a la parrilla, con queso mozzarella, salsa BBQ y verduras.',
                    'img_uri' => '/img/our-cart/hamburguesas/corralisima-3-4.jpg',
                    'pcategory_id' => $pc->id,
                    'order' => 1
                ),
                array(
                    'name' => "Corral Especial",
                    'subcategory' => "A la parrilla",
                    'description' => 'Jugosa carne 100% de res de 1/3 de libra a la parrilla, con tocineta en trozos, queso mozzarella, verduras y salsas.',
                    'img_uri' => '/img/our-cart/hamburguesas/legendaria-especial.jpg',
                    'pcategory_id' => $pc->id,
                    'order' => 1
                ),
                array(
                    'name' => "Corralera",
                    'subcategory' => "A la parrilla",
                    'description' => 'Jugosa carne 100% de res de 1/3 de libra a la parrilla, tocineta, espectacular queso colby, cebolla grillé, salsa BBQ y salsas.',
                    'img_uri' => '/img/our-cart/hamburguesas/legendaria-corralera.jpg',
                    'pcategory_id' => $pc->id,
                    'order' => 1
                ),
                array(
                    'name' => "Corral Casera",
                    'subcategory' => "A la parrilla",
                    'description' => 'Jugosa carne 100% de res de 1/3 de libra, queso americano, cebolla roja, tomate, lechuga y salsa BBQ.',
                    'img_uri' => '/img/our-cart/hamburguesas/corral-casera.jpg',
                    'pcategory_id' => $pc->id,
                    'order' => 1
                ),
            );

            DB::table('products')->insert($products);



        /** 2  Pollo
         *
         */
            $pc = ProductCategory::create([
                'name' => "Pollo",
                'order' => 2,
                'img_uri' => "/img/our-cart/pollo.jpg",
                'slug' => "pollo"
            ]);

            $products = array(
                array(
                    'name' => "Corral Pollo Mexicano",
                    'subcategory' => "Corralazos",
                    'description' => 'Jugosa pechuga de pollo de 154 gramos, frijol refrito, delicioso guacamole, tortillas de maíz, lechuga, tomate, mayonesa y pan tipo brioche.',
                    'img_uri' => '/img/our-cart/hamburguesas/corralazo-pollo-mexicano.jpg',
                    'pcategory_id' => $pc->id,
                    'order' => 1
                ),
                array(
                    'name' => "Sándwich Pollo Apanado",
                    'subcategory' => "",
                    'description' => 'Cargando...',
                    'img_uri' => '/img/our-cart/pollo/pollo-apanado2.jpg',
                    'pcategory_id' => $pc->id,
                    'order' => 1
                ),
                array(
                    'name' => "Sándwich Pollo",
                    'subcategory' => "",
                    'description' => 'Jugosa pechuga de pollo a la plancha de 154 gramos, con salsa BBQ, verduras y mayonesa.',
                    'img_uri' => '/img/our-cart/pollo/sandwich-de-pollo.jpg',
                    'pcategory_id' => $pc->id,
                    'order' => 1
                ),
                array(
                    'name' => "Sándwich pollo parrillero",
                    'subcategory' => "",
                    'description' => 'Jugosa pechuga de pollo a la parrilla de 154 gramos con salsa BBQ, pepinillos, tocineta, queso mozzarella y verduras.',
                    'img_uri' => '/img/our-cart/pollo/sandwich-de-pollo-parrillero.jpg',
                    'pcategory_id' => $pc->id,
                    'order' => 1
                ),
                array(
                    'name' => "Lomos de pollo",
                    'subcategory' => "",
                    'description' => 'Deliciosos lomos de pollo apanados y servidos en 4 ó 6 unidades acompañados de miel mostaza. Cada lomo pesa 52,4 gramos.',
                    'img_uri' => '/img/our-cart/pollo/lomos-de-pollo.jpg',
                    'pcategory_id' => $pc->id,
                    'order' => 1
                ),
            );

            DB::table('products')->insert($products);
        

        /** 3  Wraps
         *
         */
            $pc = ProductCategory::create([
                'name' => "Wraps",
                'order' => 3,
                'img_uri' => "/img/our-cart/wraps.jpg",
                'slug' => "wraps"
            ]);

            $products = array(
                array(
                    'name' => "Wrap Lomo",
                    'subcategory' => "",
                    'description' => 'Delicioso lomo a la plancha de 154 gramos, con tomate, cebolla y lechuga fresca, mayonesa y salsa ranch.',
                    'img_uri' => '/img/our-cart/Wraps/Wrap Lomo.jpg',
                    'pcategory_id' => $pc->id,
                    'order' => 1
                ),
                array(
                    'name' => "Wrap Pollo",
                    'subcategory' => "",
                    'description' => 'Deliciosa pechuga de pollo de 154 gramos asada a la parrilla, lechuga, tomate, cebolla y mayonesa.',
                    'img_uri' => '/img/our-cart/Wraps/Wrap Pollo.jpg',
                    'pcategory_id' => $pc->id,
                    'order' => 1
                ),
                array(
                    'name' => "Wrap Pollo Parrillero",
                    'subcategory' => "",
                    'description' => 'Jugosa pechuga de pollo de 154 gramos a la parrilla con salsa BBQ, pepinillos, tocineta, queso mozzarella y verduras.',
                    'img_uri' => '/img/our-cart/Wraps/Wrap Pollo.jpg',
                    'pcategory_id' => $pc->id,
                    'order' => 1
                ),
                array(
                    'name' => "Wrap Lechuga Todoterreno",
                    'subcategory' => "",
                    'description' => 'Reemplaza el pan de cualquiera de nuestras hamburguesas por wrap lechuga.',
                    'img_uri' => '/img/our-cart/Wraps/Wrap LEchuga.jpg',
                    'pcategory_id' => $pc->id,
                    'order' => 1
                ),
                array(
                    'name' => "Wrap Lechuga Pollo Parrillero",
                    'subcategory' => "",
                    'description' => 'Pechuga de pollo a la plancha de 150 gramos, tocineta , queso mozzarella, pepinillos, lechuga, tomate, cebolla y salsa BBQ, entregado en hojas de lechuga fresca.',
                    'img_uri' => '/img/our-cart/Wraps/Wrap Lechuga Pollo Parrillero.jpg',
                    'pcategory_id' => $pc->id,
                    'order' => 1
                ),
            );

            DB::table('products')->insert($products);
        
        
        
        /** 4  Ensaladas
         *
         */
            $pc = ProductCategory::create([
                'name' => "Ensaladas",
                'order' => 4,
                'img_uri' => "/img/our-cart/ensalada.jpg",
                'slug' => "ensaladas"
            ]);

            $products = array(
                array(
                    'name' => "Taco Salad",
                    'subcategory' => "",
                    'description' => 'Crujiente canastilla de hojaldre, con trozos de jamón, queso mozzarella, huevo duro, arvejas, pepinillos, cebolla, lechuga y tomate.',
                    'img_uri' => '/img/our-cart/Ensaladas/Taco Salad.jpg',
                    'pcategory_id' => $pc->id,
                    'order' => 1
                ),
                array(
                    'name' => "Ensalada Mediterránea",
                    'subcategory' => "",
                    'description' => 'Con tiernos lomitos de pollo, queso parmesano, aguacate fresco, lechuga y tomate.',
                    'img_uri' => '/img/our-cart/Ensaladas/Ensalada Mediterranea.jpg',
                    'pcategory_id' => $pc->id,
                    'order' => 1
                ),
                array(
                    'name' => "Ensalada Corral",
                    'subcategory' => "",
                    'description' => 'Con pollo a la plancha, huevo duro, jamón, queso mozzarella y parmesano, arvejas, pepinillos, maíz tierno, tomate y lechuga crujiente.',
                    'img_uri' => '/img/our-cart/Ensaladas/Ensalada Corral.jpg',
                    'pcategory_id' => $pc->id,
                    'order' => 1
                ),
                array(
                    'name' => "Ensalada COBB",
                    'subcategory' => "",
                    'description' => 'Con pollo grillé, jamón, tocineta, queso mozarella, huevo duro, aguacate, arvejas, maíz tierno, tomate y lechuga.',
                    'img_uri' => '/img/our-cart/Ensaladas/Ensalada Cob.jpg',
                    'pcategory_id' => $pc->id,
                    'order' => 1
                ),
            );

            DB::table('products')->insert($products);
        
        
        

        /** 5  Vaqueros
         *
         */
            $pc = ProductCategory::create([
                'name' => "Vaqueros",
                'order' => 5,
                'img_uri' => "/img/our-cart/vaquero.jpg",
                'slug' => "vaqueros"
            ]);

            $products = array(
                array(
                    'name' => "Vaquero Sencillo",
                    'subcategory' => "",
                    'description' => 'Deliciosa salchicha a la parrilla en pan perro, con cebolla picada, papas chips trituradas y salsas.',
                    'img_uri' => '/img/our-cart/Vaqueros/Sencillo.jpg',
                    'pcategory_id' => $pc->id,
                    'order' => 1
                ),
                array(
                    'name' => "Vaquero Especial",
                    'subcategory' => "",
                    'description' => 'Deliciosa salchicha a la parrilla en pan perro, con queso mozzarella, tocineta, cebolla picada, papas chips trituradas  y salsas.',
                    'img_uri' => '/img/our-cart/Vaqueros/Especial.jpg',
                    'pcategory_id' => $pc->id,
                    'order' => 1
                ),
                array(
                    'name' => "Vaquero Queso",
                    'subcategory' => "",
                    'description' => 'Deliciosa salchicha a la parrilla en pan perro, con queso mozzarella, cebolla picada, papas chips trituradas y salsas.',
                    'img_uri' => '/img/our-cart/Vaqueros/Queso.jpg',
                    'pcategory_id' => $pc->id,
                    'order' => 1
                ),
                array(
                    'name' => "Vaquero Mexicano",
                    'subcategory' => "",
                    'description' => 'Deliciosa salchicha a la parrilla en pan perro, frijol refrito, guacamole y tortillas de maíz.',
                    'img_uri' => '/img/our-cart/Vaqueros/Mexicano.jpg',
                    'pcategory_id' => $pc->id,
                    'order' => 1
                ),
                array(
                    'name' => "Vaquero Hawaiano",
                    'subcategory' => "",
                    'description' => 'Deliciosa salchicha a la parrilla en pan perro, queso mozzarella, deliciosa piña, papas chips trituradas y salsas.',
                    'img_uri' => '/img/our-cart/Vaqueros/Hawaiano.jpg',
                    'pcategory_id' => $pc->id,
                    'order' => 1
                ),
            );

            DB::table('products')->insert($products);
        /** 6  Corralito Infantil
         *
         */
            $pc = ProductCategory::create([
                'name' => "Corralito Infantil",
                'order' => 6,
                'img_uri' => "/img/our-cart/corralito.jpg",
                'slug' => "corralito-infantil"
            ]);

            $products = array(
                array(
                    'name' => "Corralito Hamburguesa",
                    'subcategory' => "",
                    'description' => 'Carne de 90 gramos, papas a la francesa, jugo o gaseosa pequeña acompañado de un sencillo.',
                    'img_uri' => '/img/our-cart/Corralito Infantil/Corralito Hamburguesa.jpg',
                    'pcategory_id' => $pc->id,
                    'order' => 1
                ),
                array(
                    'name' => "Corralito Vaquero",
                    'subcategory' => "",
                    'description' => 'Vaquero, papas a la francesa, jugo o gaseosa pequeña acompañado de un sencillo.',
                    'img_uri' => '/img/our-cart/Corralito Infantil/Corralito Perro.jpg',
                    'pcategory_id' => $pc->id,
                    'order' => 1
                ),
                array(
                    'name' => "Corralito lomos",
                    'subcategory' => "",
                    'description' => 'Lomitos de pollo, papas a la francesa, jugo o gaseosa pequeña acompañado de un sencillo. Cada lomo pesa 52,4 gramos.',
                    'img_uri' => '/img/our-cart/Corralito Infantil/Corralito Lomitos.jpg',
                    'pcategory_id' => $pc->id,
                    'order' => 1
                ),
            );

            DB::table('products')->insert($products);


        /** 7  Pa' Tardear
         *
         */
            $pc = ProductCategory::create([
                'name' => "Pa' Tardear",
                'order' => 9,
                'img_uri' => "/img/our-cart/pa-tardear.jpg",
                'slug' => "pa-tardear"
            ]);

            $products = array(
                array(
                    'name' => "Vaquero salsa pepinillos",
                    'subcategory' => "",
                    'description' => 'Pan artesanal acompañado de salchicha de 50 gr., queso mozzarella y salsa de pepinillos.',
                    'img_uri' => '/img/our-cart/Pa tardear/Vaquero Pepinillos.jpg',
                    'pcategory_id' => $pc->id,
                    'order' => 1
                ),
                array(
                    'name' => "Wrap Philadelphia",
                    'subcategory' => "",
                    'description' => '2 tortillas integrales medianas, 65 gr. de lomo, cebolla grillé y queso americano.',
                    'img_uri' => '/img/our-cart/Pa tardear/WRap Philadelphia.jpg',
                    'pcategory_id' => $pc->id,
                    'order' => 1
                ),
                array(
                    'name' => "Malteada con papas",
                    'subcategory' => "",
                    'description' => 'Deliciosa malteada de 12 oz del sabor que elijas acompañada de unas papas a la francesa medianas.',
                    'img_uri' => '/img/our-cart/Pa tardear/Malteada.jpg',
                    'pcategory_id' => $pc->id,
                    'order' => 1
                ),
                array(
                    'name' => "Papas con queso cheddar y tocineta",
                    'subcategory' => "",
                    'description' => 'Papas medianas, queso fundido y tocineta.',
                    'img_uri' => '/img/our-cart/Pa tardear/Papas Tocineta.jpg',
                    'pcategory_id' => $pc->id,
                    'order' => 1
                ),
                array(
                    'name' => "Vaquero Tocineta",
                    'subcategory' => "",
                    'description' => 'Pan artesanal acompañado de salchicha de 50 gr., queso mozzarella papas perro y tocineta',
                    'img_uri' => '/img/our-cart/Pa tardear/Vaquero Tocineta.jpg',
                    'pcategory_id' => $pc->id,
                    'order' => 1
                ),
            );

            DB::table('products')->insert($products);


        /** 8  Acompañamientos
         *
         */
            $pc = ProductCategory::create([
                'name' => "Acompañamientos",
                'order' => 7,
                'img_uri' => "/img/our-cart/acompañamiento.jpg",
                'slug' => "acompañamientos"
            ]);

            $products = array(
                array(
                    'name' => "Anillos de cebolla",
                    'subcategory' => "",
                    'description' => 'Crujientes anillos de cebolla apanados',
                    'img_uri' => '/img/our-cart/Acompañamientos/Aros.jpg',
                    'pcategory_id' => $pc->id,
                    'order' => 1
                ),
                array(
                    'name' => "Papas a la francesa",
                    'subcategory' => "",
                    'description' => 'Las papas más crocantes y deliciosas para que armes tu menú como más te guste.',
                    'img_uri' => '/img/our-cart/Acompañamientos/Papas a la Francesa.jpg',
                    'pcategory_id' => $pc->id,
                    'order' => 1
                ),
                array(
                    'name' => "Papas en cascos",
                    'subcategory' => "",
                    'description' => 'Deliciosas y crocantes papas con cáscara para que armes tu menú como más te guste.',
                    'img_uri' => '/img/our-cart/Acompañamientos/Casquitos.jpg',
                    'pcategory_id' => $pc->id,
                    'order' => 1
                ),
                array(
                    'name' => "Papas en espiral",
                    'subcategory' => "",
                    'description' => 'Crocantes papas en espiral para que armes tu menú como más te guste.',
                    'img_uri' => '/img/our-cart/Acompañamientos/Papas Espiral.jpg',
                    'pcategory_id' => $pc->id,
                    'order' => 1
                ),
                array(
                    'name' => "Yucas con suero",
                    'subcategory' => "",
                    'description' => 'Crocantes yucas acompañadas de un delicioso suero costeño.',
                    'img_uri' => '/img/our-cart/Acompañamientos/Yucas.jpg',
                    'pcategory_id' => $pc->id,
                    'order' => 1
                ),
            );
            DB::table('products')->insert($products);


        /** 9  Adiciones
         *
         */
            $pc = ProductCategory::create([
                'name' => "Adiciones",
                'order' => 8,
                'img_uri' => "/img/our-cart/Adiciones.jpg",
                'slug' => "adiciones"
            ]);

            $products = array(
                array(
                    'name' => "Suero costeño",
                    'subcategory' => "",
                    'description' => '',
                    'img_uri' => '/img/our-cart/Adiciones/Suero Costeño.jpg',
                    'pcategory_id' => $pc->id,
                    'order' => 1
                ),
                array(
                    'name' => "Tocineta",
                    'subcategory' => "",
                    'description' => '',
                    'img_uri' => '/img/our-cart/Adiciones/Tocineta.jpg',
                    'pcategory_id' => $pc->id,
                    'order' => 1
                ),
                array(
                    'name' => "Cebolla Grillé",
                    'subcategory' => "",
                    'description' => '',
                    'img_uri' => '/img/our-cart/Adiciones/Cebolla Grille.jpg',
                    'pcategory_id' => $pc->id,
                    'order' => 1
                ),
                array(
                    'name' => "Queso Fundido",
                    'subcategory' => "",
                    'description' => '',
                    'img_uri' => '/img/our-cart/Adiciones/Queso.jpg',
                    'pcategory_id' => $pc->id,
                    'order' => 1
                ),
                array(
                    'name' => "Pepinillos",
                    'subcategory' => "",
                    'description' => '',
                    'img_uri' => '/img/our-cart/Adiciones/Pepinillos.jpg',
                    'pcategory_id' => $pc->id,
                    'order' => 1
                ),
                array(
                    'name' => "Otras Adiciones",
                    'subcategory' => "",
                    'description' => '',
                    'img_uri' => '/img/our-cart/Adiciones/Extras.jpg',
                    'pcategory_id' => $pc->id,
                    'order' => 1
                ),
            );

            DB::table('products')->insert($products);

 
        

        /** 10 Malteadas y Conos
         *
         */
            $pc = ProductCategory::create([
                'name' => "Malteadas y Conos",
                'order' => 10,
                'img_uri' => "/img/our-cart/malteada.jpg",
                'slug' => "malteadas-y-conos"
            ]);

            $products = array(
                array(
                    'name' => "Maracuyá",
                    'subcategory' => "",
                    'description' => 'Preparadas con helado gourmet Von Glace y salsa de maracuyá con semillas de amapola.',
                    'img_uri' => '/img/our-cart/Helados y Malteadas/Malteada-Maracuya.jpg',
                    'pcategory_id' => $pc->id,
                    'order' => 1
                ),
                array(
                    'name' => "Frutos del bosque",
                    'subcategory' => "",
                    'description' => 'Preparadas con helado gourmet Von Glacet y salsa de mora. Puedes disfrutarla en su version Ligth.',
                    'img_uri' => '/img/our-cart/Helados y Malteadas/Malteada Frutos.jpg',
                    'pcategory_id' => $pc->id,
                    'order' => 1
                ),
                array(
                    'name' => "Macadamia",
                    'subcategory' => "",
                    'description' => 'Preparadas con helado gourmet Von Glacet y nueces de macadamia.',
                    'img_uri' => '/img/our-cart/Helados y Malteadas/Malteada Macadamia.jpg',
                    'pcategory_id' => $pc->id,
                    'order' => 1
                ),
                array(
                    'name' => "Vainilla",
                    'subcategory' => "",
                    'description' => 'Preparadas con helado gourmet Von Glacet y ralladura de chocolate.',
                    'img_uri' => '/img/our-cart/Helados y Malteadas/Malteada Vainilla.jpg',
                    'pcategory_id' => $pc->id,
                    'order' => 1
                ),
                array(
                    'name' => "Chocolate",
                    'subcategory' => "",
                    'description' => 'Preparadas con helado gourmet Von Glacet y ralladura de chocolate. Puedes disfrutarla en su version Ligth.',
                    'img_uri' => '/img/our-cart/Helados y Malteadas/Malteada Chocolate.jpg',
                    'pcategory_id' => $pc->id,
                    'order' => 1
                ),
                array(
                    'name' => "Café Mocca",
                    'subcategory' => "",
                    'description' => "Malteada de café mocca con salsa de chocolate y topping de galleta oreo triturada. Puedes disfrutarla en su version Café.",
                    'img_uri' => '/img/our-cart/Helados y Malteadas/Malteada-Cafe-Moka.jpg',
                    'pcategory_id' => $pc->id,
                    'order' => 1
                ),
                array(
                    'name' => "Frutos del bosque",
                    'subcategory' => "",
                    'description' => 'Puedes disfrutarla en su version Ligth.',
                    'img_uri' => '/img/our-cart/Helados y Malteadas/Helado Frutos.jpg',
                    'pcategory_id' => $pc->id,
                    'order' => 1
                ),
                array(
                    'name' => "Macadamia",
                    'subcategory' => "",
                    'description' => '',
                    'img_uri' => '/img/our-cart/Helados y Malteadas/Helado Macadamia.jpg',
                    'pcategory_id' => $pc->id,
                    'order' => 1
                ),
                array(
                    'name' => "Vainilla",
                    'subcategory' => "",
                    'description' => 'Puedes disfrutarla en su version Ligth.',
                    'img_uri' => '/img/our-cart/Helados y Malteadas/Helado Vainilla.jpg',
                    'pcategory_id' => $pc->id,
                    'order' => 1
                ),
                array(
                    'name' => "Maracuyá",
                    'subcategory' => "",
                    'description' => '',
                    'img_uri' => '/img/our-cart/Helados y Malteadas/Helado Maracuya.jpg',
                    'pcategory_id' => $pc->id,
                    'order' => 1
                ),
                array(
                    'name' => "Chocolate",
                    'subcategory' => "",
                    'description' => 'Puedes disfrutarla en su version Ligth.',
                    'img_uri' => '/img/our-cart/Helados y Malteadas/Helado Chocolate.jpg',
                    'pcategory_id' => $pc->id,
                    'order' => 1
                ),/*
                array(
                    'name' => "Coco y Limon",
                    'subcategory' => "",
                    'description' => '',
                    'img_uri' => '/img/our-cart/Helados y Malteadas/Helado Coco.jpg',
                    'pcategory_id' => $pc->id,
                    'order' => 1
                ),*/
                array(
                    'name' => "Fresa",
                    'subcategory' => "",
                    'description' => '',
                    'img_uri' => '/img/our-cart/Helados y Malteadas/Helado Fresa.jpg',
                    'pcategory_id' => $pc->id,
                    'order' => 1
                ),
                array(
                    'name' => "Doble",
                    'subcategory' => "",
                    'description' => 'Porque dos son mejor que una ¡Pídelo doble!.',
                    'img_uri' => '/img/our-cart/Helados y Malteadas/Cono-Doble.jpg',
                    'pcategory_id' => $pc->id,
                    'order' => 1
                ),
            );

            DB::table('products')->insert($products);

        /** 11 Postres 
         *
         */
            $pc = ProductCategory::create([
                'name' => "Postres",
                'order' => 11,
                'img_uri' => "/img/our-cart/postre.jpg",
                'slug' => "postres"
            ]);

            $products = array(
                array(
                    'name' => "Sundae",
                    'subcategory' => "",
                    'description' => 'Exquisito helado del sabor que prefieras bañado en salsa de caramelo y chocolate. Decorado con nueces en almíbar y una jugosa cereza.',
                    'img_uri' => '/img/our-cart/Helados y Malteadas/Sundae.jpg',
                    'pcategory_id' => $pc->id,
                    'order' => 1
                ),
                array(
                    'name' => "Pie con helado",
                    'subcategory' => "",
                    'description' => 'Crocante pie relleno de manzana o limón, acompañado de una bola de helado.',
                    'img_uri' => '/img/our-cart/Helados y Malteadas/Pie de Limon.jpg',
                    'pcategory_id' => $pc->id,
                    'order' => 1
                ),
                array(
                    'name' => "Brownie con helado",
                    'subcategory' => "",
                    'description' => 'Con cobertura de chocolate o arequipe, acompañado de una bola de helado y hojuelas de chocolate.',
                    'img_uri' => '/img/our-cart/Helados y Malteadas/Brownie.jpg',
                    'pcategory_id' => $pc->id,
                    'order' => 1
                ),
            );

            DB::table('products')->insert($products);
        /** 12 Desayunos
         *
         */
            $pc = ProductCategory::create([
                'name' => "Desayunos",
                'order' => 12,
                'img_uri' => "/img/our-cart/desayunos.jpg",
                'slug' => "desayunos"
            ]);

            $products = array(
                array(
                    'name' => "Desayuno Tradicional",
                    'subcategory' => "",
                    'description' => 'Huevos revueltos al estilo Corral servidos en pan, arepa o wrap. Acompáñalos con un exquisito café y/o jugo de naranja recién preparado.',
                    'img_uri' => '/img/our-cart/Desayunos/Desayuno_tradicional.jpg',
                    'pcategory_id' => $pc->id,
                    'order' => 1
                ),
                array(
                    'name' => "Desayuno granjero",
                    'subcategory' => "",
                    'description' => 'Deliciosos huevos revueltos al estilo Corral con jamón y queso servidos en pan, arepa o wrap. Acompáñalos con un exquisito café y/o jugo de naranja recién preparado.',
                    'img_uri' => '/img/our-cart/Desayunos/Granjero.jpg',
                    'pcategory_id' => $pc->id,
                    'order' => 1
                ),
                array(
                    'name' => "Desayuno súper",
                    'subcategory' => "",
                    'description' => 'Deliciosos huevos revueltos al estilo Corral con jamón, queso, lechuga y tomate, servidos en pan, arepa o wrap. Acompáñalos con un exquisito café y/o jugo de naranja recién preparado.',
                    'img_uri' => '/img/our-cart/Desayunos/Desyuno Super.jpg',
                    'pcategory_id' => $pc->id,
                    'order' => 1
                ),
                array(
                    'name' => "Huevos corral",
                    'subcategory' => "",
                    'description' => 'Deliciosos huevos revueltos preparados con carne al estilo Corral servidos en pan, arepa o wrap. Acompáñalos con un exquisito café y/o jugo de naranja recién preparado.',
                    'img_uri' => '/img/our-cart/Desayunos/Corral.jpg',
                    'pcategory_id' => $pc->id,
                    'order' => 1
                ),
                array(
                    'name' => "Huevos criollos",
                    'subcategory' => "",
                    'description' => 'Deliciosos huevos revueltos preparados con cebolla y tomate al estilo Corral servidos en pan, arepa o wrap. Acompáñalos con un exquisito café y/o jugo de naranja recién preparado.',
                    'img_uri' => '/img/our-cart/Desayunos/Criollos.jpg',
                    'pcategory_id' => $pc->id,
                    'order' => 1
                ),
                array(
                    'name' => "Huevos rancheros",
                    'subcategory' => "",
                    'description' => 'Deliciosos huevos revueltos preparados con tocineta y maíz al estilo Corral servidos en pan, arepa o wrap. Acompáñalos con un exquisito café y/o jugo de naranja recién preparado.',
                    'img_uri' => '/img/our-cart/Desayunos/Desayuno Ranchero.jpg',
                    'pcategory_id' => $pc->id,
                    'order' => 1
                ),
                array(
                    'name' => "Huevos vaqueros",
                    'subcategory' => "",
                    'description' => 'Deliciosos huevos revueltos preparados con salchicha al estilo Corral servidos en pan, arepa o wrap. Acompáñalos con un exquisito café y/o jugo de naranja recién preparado.',
                    'img_uri' => '/img/our-cart/Desayunos/Vaqueros.jpg',
                    'pcategory_id' => $pc->id,
                    'order' => 1
                ),
            );

            DB::table('products')->insert($products);


        /** 13 Para Compartir
         *
         */
            $pc = ProductCategory::create([
                'name' => "Para Compartir",
                'order' => 13,
                'img_uri' => "/img/our-cart/para-compartir.jpg",
                'slug' => "para-compartir"
            ]);

            $products = array(
                array(
                    'name' => "Lomo Parrilla",
                    'subcategory' => "",
                    'description' => 'Jugoso lomo de res de 900 gr, acompañado por papa, mazorca y salsas. (Disponible en algunos restaurantes de la cadena).',
                    'img_uri' => '/img/our-cart/Para Compartir/Lomo Parrilla.jpg',
                    'pcategory_id' => $pc->id,
                    'order' => 1
                ),
                array(
                    'name' => "Picada Campestre",
                    'subcategory' => "",
                    'description' => 'Deliciosa pechuga de pollo, lomo de res, guacamole, tomate, salsa BBQ.',
                    'img_uri' => '/img/our-cart/Para Compartir/Picada Campestre.jpg',
                    'pcategory_id' => $pc->id,
                    'order' => 1
                ),
            );

            DB::table('products')->insert($products);

    }
}