<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RestaurantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('restaurants', function (Blueprint $table) {
            $table->increments('id');
            $table->string('cost_center')->unique()->nullable();
            $table->string('location_name');
            $table->string('address');
            $table->string('city');
            $table->string('lat');
            $table->string('lng');
            $table->string('schedule')->nullable();
            $table->boolean('wifi_zone')->nullable();
            $table->boolean('pet_friendly');
            $table->boolean('kid_zone');
            $table->boolean('parking');
            $table->boolean('valet_parking');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });					

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('restaurants');
    }
}
