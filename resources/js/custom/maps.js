$(function() {
    __map = null;
    __initialCenter = null;
    __markers = [];
    __excludedIds = [];
    __capitalZones = [];
    __capitalZones["Zona Norte"] = { lat: parseFloat(4.723638888888889), lng: parseFloat(-74.04138888888889) }
    __capitalZones["Zona Noroccidente"] = { lat: parseFloat(4.734555555555556), lng: parseFloat(-74.08816666666667) }
    __capitalZones["Zona Occidente"] = { lat: parseFloat(4.642972222222221), lng: parseFloat(-74.1201111111111) }
    __capitalZones["Zona Sur"] = { lat: parseFloat(4.588277777777778), lng: parseFloat(-74.14422222222223) }
    __capitalZones["Zona Chapinero"] = { lat: parseFloat(4.654444444444445), lng: parseFloat(-74.06155555555556) }
    __capitalZones["Zona Centro"] = { lat: parseFloat(4.604972222222222), lng: parseFloat(-74.08011111111111) }
    __capitalZonesDef = [];
    __capitalZonesDef["Zona Norte"] = "Desde la Calle 114 hasta la 230, desde la séptima a la carrera 54"
    __capitalZonesDef["Zona Noroccidente"] = "Desde la Calle 80 hasta la 152, desde la Boyacá hasta la 110"
    __capitalZonesDef["Zona Occidente"] = "Desde la avenida de las Américas hasta la avenida calle 26, desde la carrera 68 hasta la carrera 94"
    __capitalZonesDef["Zona Sur"] = "Desde la Carrera 19C hasta la avenida de las Américas, desde la Carrera 30 hasta la 72"
    __capitalZonesDef["Zona Chapinero"] = "Desde la calle 13 hasta la calle 80, desde la circunvalar hasta la carrera 60 "
    __capitalZonesDef["Zona Centro"] = "Desde la Calle 1 hasta la calle 45, desde la carrera cuarta hasta la carrera 35"

    getLocation();

    function initMap(position = null) {
        var __lat = position ? position.coords.latitude : 4.6483228507399925;
        var __lng = position ? position.coords.longitude : -74.10780699999998;
        map = new google.maps.Map(document.getElementById('map'), {
            zoom: 15,
            center: { lat: __lat, lng: __lng }
        });
        __map = map
        __initialCenter = __map.getCenter();
        addMarkers(__map)
        google.maps.event.addListener(map, 'dragend', function(event) {
            oldCenter = __initialCenter
            newCenter = __map.getCenter();

            if ((calcDistance(oldCenter, newCenter)) > 5) {
                // Do something
                __initialCenter = newCenter
                addMarkers(__map)
            } else {}
        });
    }

    function getLocation() {
        navigator.geolocation.getCurrentPosition(initMap, initMap());
        /* if (navigator.geolocation) {
             navigator.geolocation.getCurrentPosition(initMap);
         } else {
             initMap()
         }*/
    }

    function calcDistance(p1, p2) {
        return (google.maps.geometry.spherical.computeDistanceBetween(p1, p2) / 1000).toFixed(2);
    }

    function showInfo(id) {
        mark = __markers.find(obj => {
            return obj.id === id
        })

        $("#name").text(mark.location_name)
        $("#address").text(mark.address)

        $("#box").css('height', 'auto')
        $("#schedule").text("Horarios: " + mark.schedule)

        $("#services").css('display', 'inherit')
        mark.pet_friendly ? $("#pet_friendly").text('Acepta Mascotas').css('display','inherit') : $("#pet_friendly").css('display','none');
        mark.kid_zone ? $("#kid_zone").text('Tiene Zona Infantil').css('display','inherit') : $("#kid_zone").css('display','none');
        mark.parking ? $("#parking").text('Tiene Parqueadero').css('display','inherit') : $("#parking").css('display','none');
        mark.valet_parking ? $("#valet_parking").text('Tiene Valet Parking').css('display','inherit') : $("#valet_parking").css('display','none');
    }

    function addMarkers(map) {
        $.ajax({
            url:    '/endpoints/get-restaurants',
            type:   'get',
            data: {
                'lat': __initialCenter.lat(),
                'lng': __initialCenter.lng(),
                'excluded': null //__excludedIds
            },
            success:   function(response) {
                $('#markers').find('option').not('#Fone').remove();
                $.each(response.restaurants, function(index, mark) {
                    __markers.push(mark);
                    //__excludedIds.push(mark.id);
                    $("#markers").append(new Option(mark.location_name, mark.id));
                    marker = new google.maps.Marker({
                        position: { lat: parseFloat(mark.lat), lng: parseFloat(mark.lng) },
                        map: map,
                        nid: mark.id,
                        icon: "/img/restaurants/icono-map.png",
                        title: mark.title,
                        animation: google.maps.Animation.DROP,
                        zIndex: 3
                    });
                    google.maps.event.addListener(marker, "click", function() {
                        showInfo(this.nid);
                        map.setCenter(this.getPosition());
                        //this.setAnimation(google.maps.Animation.BOUNCE);

                    });
                })
            }
        }).fail(function(xhr, status, error, errors) {
            console.log(error)
        });

    }

    //    $("#cities").on("change", function() {
    //      __map
    //})

    $("#markers").change(function() {
        id = parseInt($("#markers").val());
        marker = __markers.find(obj => {
            return obj.id === id
        })
        __map.panTo({ lat: parseFloat(marker.lat), lng: parseFloat(marker.lng) });
        showInfo(marker.id);

    });

    $("#zones").change(function() {
        __map.panTo(__capitalZones[$("#zones").val()])
        __initialCenter = __map.getCenter();
        addMarkers(__map)
        console.log(__capitalZonesDef[$("#zones").val()])
        $("#zoneDef").text(__capitalZonesDef[$("#zones").val()])
    })
});

$("#cities").change(function() {
    $.ajax({
        url:    '/endpoints/get-restaurants',
        type:   'get',
        data: {
            'city': $("#cities").val(),
        },
        success:   function(response) {
            $('#markers').find('option').not('#Fone').remove();
            first = true
            $.each(response.restaurants, function(index, mark) {
                __markers.push(mark);
                //__excludedIds.push(mark.id);
                $("#markers").append(new Option(mark.location_name, mark.id));
                marker = new google.maps.Marker({
                    position: { lat: parseFloat(mark.lat), lng: parseFloat(mark.lng) },
                    map: map,
                    nid: mark.id,
                    icon: "/img/restaurants/icono-map.png",
                    title: mark.title,
                    animation: google.maps.Animation.DROP,
                    zIndex: 3
                });
                google.maps.event.addListener(marker, "click", function() {
                    showInfo(this.nid);
                    map.setCenter(this.getPosition());
                    //this.setAnimation(google.maps.Animation.BOUNCE);

                });
                if (first) {
                    __map.panTo(marker.position)
                }
                first = false;
            })
        }
    }).fail(function(xhr, status, error, errors) {
        console.log(error)
    });

    $("#zones").empty();
    if ($("#cities").val().toLocaleLowerCase() == "bogotá, d.c.") {
        $("#zones").removeAttr('disabled');
        $("#zones").append('<option selected disabled hidden>Selecciona una Zona</option>');
        $("#zones").append(new Option('Zona Norte', "Zona Norte")); 
        $("#zones").append(new Option('Zona Noroccidente', "Zona Noroccidente")); 
        $("#zones").append(new Option('Zona Occidente', "Zona Occidente")); 
        $("#zones").append(new Option('Zona Sur', "Zona Sur")); 
        $("#zones").append(new Option('Zona Chapinero', "Zona Chapinero")); 
        $("#zones").append(new Option('Zona Centro', "Zona Centro")); 
    } else {
        $("#zones").append(new Option('Aplica sólo a Bogotá', "null", true, true)); 
        $("#zones").prop('disabled', 'disabled');
        $("#zoneDef").text("")
    } 
})

function showInfo(id) {
    mark = __markers.find(obj => {
        return obj.id === id
    })

    $("#name").text(mark.location_name)
    $("#address").text(mark.address)

    $("#box").css('height', 'auto')
    $("#schedule").text("Horarios: " + mark.schedule)

    $("#services").css('display', 'inherit')
    $("#pet_friendly").text(mark.pet_friendly ? 'Acepta Mascotas' : 'No acepta mascotas')
    $("#kid_zone").text(mark.kid_zone ? 'Tiene Zona Infantil' : 'No tiene zona infantil')
    $("#parking").text(mark.parking ? 'Tiene Parqueadero' : 'No tiene parqueadero')
    $("#valet_parking").text(mark.valet_parking ? 'Tiene Valet Parking' : 'No tiene valet parking')
}