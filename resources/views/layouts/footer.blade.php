<div class="footer">
    <div>
        <img src="/img/logo/footerlogo.png" alt="Restaurant" />
        <br>
        <span>
            El Corral &copy; {{ date('Y') }} 
            <br> Todos los derechos reservados 
            <br> Desarrollado por 
            <a href="https://www.ad-in.com.co" target="__blank">AdIn</a>
        </span>
    </div>
    <div>
        <h4>Acerca de El Corral</h4>
        <ul>
            <li><a href="/nosotros">Nosotros</a></li>
            <li><a href="https://pideenlinea.elcorral.com/?source=pagElCorral" target="__blank">Domicilios en Línea</a></li>
            {{-- <li><a href="/horarios-para-llevar" target="__blank">Servicios Restaurantes</a></li> --}}
            <li><a href="/restaurantes">Restaurantes</a></li>
            <li><a href="/nuestra-carta">Nuestra Carta</a></li>
            <li><a href="/lo-nuevo">Lo Nuevo</a></li>
            
        </ul>
    </div>
    <div>
        <h4>Ayuda</h4>
        <ul>
            <li><a href="/hablanos">Háblanos</a></li>
            <li><a href="https://aplicaciones.alimentosalconsumidor.com/itmanager/portal-proveedores/login.html" target="__blank">Certificados</a></li>
            <li><a href="https://www.magneto365.com/aco" target="__blank">Trabaja con Nosotros</a></li>
            <li><a href="/pdf/Politica-de-Tratamiento-de-Datos-Personales.pdf" target="__blank">Política de Tratamiento de Datos</a></li>
            <li><a href="/terminos-y-condiciones" target="__blank">Terminos y Condiciones</a></li>
        </ul>
    </div>
    <div>
        <h4>Síguenos</h4>
        <ul>
            <li>
                <a href="https://www.facebook.com/hamburguesaselcorral/" target="__blank" class="sprite fb" title="Facebook El Corral">
                </a>
                <a href="https://www.instagram.com/elcorraloficial/" target="__blank" class="sprite ig" title="Instagram El Corral">
                </a>
                <a href="https://twitter.com/elcorral_?lang=es" target="__blank" class="sprite tw" title="Twitter El Corral">
                </a>
                <a href="https://www.youtube.com/user/HamburguesasElCorral" target="__blank" class="sprite yt" title="Youtube El Corral">
                </a>
                <a href="https://vm.tiktok.com/KHGpdC/" target="__blank" class="sprite tt" title="TikTok El Corral">
                </a>
            </li>
        </ul>
    </div>
</div>
    