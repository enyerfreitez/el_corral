@php
 $default_title = "Hamburguesas, malteadas, helados y más | El Corral";
 $default_meta_des = "Disfruta del delicioso sabor de nuestras hamburguesas, perros calientes, ensaladas, wraps, papas a la francesa, malteadas, helados y mucho más ¡Entra ya!";
@endphp
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title id="page_title">{{isset($title) ? $title : $default_title}}</title>
    <!-- mobile setup -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=5" />
    <meta name="apple-mobile-web-app-title" content="El Corral &middot; Hamburguesas, Malteadas y Papitas" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- preloads -->
    <link rel="preload" href="/css/style.min.css" as="style">
    <link rel="preload" href="/js/libs/jquery-2.2.3.js" as="script">
    <link rel="preload" href="/js/lazysizes.min.js" as="script">
    <link rel="preload" href="/js/custom/slider.js" as="script">
    <link rel="preload" href="/js/libs/modernizr.js" as="script">
    <link rel="preload" href="/js/libs/page.js" as="script">
    <link rel="preload" href="/js/libs/device.js" as="script">
    <link rel="preload" href="/js/libs/jquery.rcrumbs.min.js" as="script">
    <link rel="preload" href="/js/custom/slidenavigation.js" as="script">
    <link rel="preload" href="/js/app.js" as="script">
    <link rel="preload" href="/js/custom/legals.min.js" as="script">
    <!-- stylesheet -->
    <link rel="stylesheet" href="/css/style.min.css?map={{date('ymdhis')}}">
    {{-- <link rel="stylesheet" href="/css/covid.css"> --}}
    <!-- Page description -->
    <meta class="meta-des" name="description" content="{{ isset($description) ? $description : $default_meta_des }}" />
    <meta name="keywords" content="Hamburguesas, Receta Original">
    <!-- Facebook -->
    <meta class="meta-title" property="og:title" content="{{isset($title) ? $title : $default_title}}" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="{{URL::current()}}" />
    <meta property="og:image" content="{{URL::current() . "/img/logo/logo_old.png"}}" />
    <meta class="meta-des" property="og:description" content="{{ isset($description) ? $description : $default_meta_des }}" />

    <!-- Twitter -->
    <meta class="meta-title" name="twitter:title" content="{{isset($title) ? $title : $default_title}}" />
    <meta class="meta-des" name="twitter:description" content="{{ isset($description) ? $description : $default_meta_des }}" />
    <!-- Favicons -->
    <link rel="apple-touch-icon" sizes="72x72" href="/img/favicons/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/img/favicons/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/img/favicons/apple-touch-icon-152x152.png">
    <link rel="icon" type="image/png" href="/img/favicons/favicon.png" sizes="96x96">
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-77148445-1"></script>
    <!--script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-77148445-1');
    </script-->
    <script async src='https://www.google-analytics.com/analytics.js'></script>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-TLP8D7T');</script>
    <!-- End Google Tag Manager -->
    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};
        if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
        n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t,s)}(window,document,'script',
        'https://connect.facebook.net/en_US/fbevents.js');
         fbq('init', '2412092759046700'); 
        fbq('track', 'PageView');
    </script>
    <noscript>
        <img height="1" width="1" 
        src="https://www.facebook.com/tr?id=2412092759046700&ev=PageView
        &noscript=1"/>
    </noscript>
    <!-- End Facebook Pixel Code -->
    @if(!isset($schema))
    <script type="application/ld+json"> 
        {
          "@context": "https://schema.org",
          "@type": "Corporation",
          "name": "El Corral",
          "url": "https://www.elcorral.com",
          "logo": "https://www.elcorral.com/img/logo/logo.png",
          "alternateName": "Hamburguesas El Corral",
          "slogan": "La receta original",
          "parentOrganization": "Grupo Nutresa",
          "telephone": "018000114722",
          "sameAs": [
            "https://www.facebook.com/hamburguesaselcorral/",
            "https://www.instagram.com/elcorraloficial/",
            "https://twitter.com/elcorral_?lang=es",
            "https://www.youtube.com/user/HamburguesasElCorral",
            "https://vm.tiktok.com/KHGpdC/"
          ],
          "contactPoint": [
            {
              "@type": "ContactPoint",
              "telephone": "018000114722",
              "contactType": "customer service",
              "areaServed": "CO",
              "availableLanguage": "es"
            }
          ]
        }
    </script>
    @else
    {!! $schema !!}
    @endif


    <!-- Hotjar Tracking Code for https://www.elcorral.com/ -->
    <script>
        (function(h,o,t,j,a,r){
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:1847316,hjsv:6};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
        })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
    </script>
</head>

<body>
    <!-- Top shadow -->
    <div class="shadow"></div>
    <!-- end top shadow -->

    <!-- The splash screen -->
    <div id="splash">
        <div class="loader">
            <img class="splash-logo" src="/img/logo/logo.png" alt="Logo El Corral"/>
            <div class="line"></div>
        </div>
    </div>
    <!-- End of splash screen -->
    <header class="header">
        <a href="https://pideenlinea.elcorral.com/?source=pagElCorral" class="ecommerce" target="__blank" onclick="gtag('event', 'Clic', {'event_category': 'Mosca domicilios ', 'event_label': 'Mosca domicilios'});">
            <img src="/img/logo/Boton-Domicilios2.png" alt="Domicilios El Corral">
        </a>
        <div class="hammenu" onclick="$('main').animate({scrollTop: $('.strokes').offset().top},'slow');" id="menuIcon" style="cursor:pointer">
            <p>
                Menú
            </p>
            <img src="/img/menu.png" alt="menú">
        </div>
        <div class="menu" id="menuItems">
            <img src="/img/menu.png" alt="menú" id="d-menu">
            <ul id="d-list">
                <li><a class="d-menu-item" href="https://pideenlinea.elcorral.com/?source=pagElCorral" target="__blank">Domicilios Pide en Línea </a> </li>
                {{-- <li><a class="d-menu-item" href="/horarios-para-llevar"> Servicios Restaurantes</a> </li> --}}
                <li><a class="d-menu-item" href="/restaurantes"> Restaurantes </a> </li>
                <li><a class="d-menu-item" href="/nuestra-carta"> Nuestra Carta </a> </li>
                <li><a class="d-menu-item" href="/lo-nuevo"> Lo Nuevo </a> </li>
                <li><a class="d-menu-item" href="/hablanos"> Háblanos </a> </li>
                <li><a class="d-menu-item" href="/eventos-fiestas-y-bonos"> Eventos, Fiestas y Bonos </a> </li>
                <li><a class="d-menu-item" href="/sostenibilidad"> Sostenibilidad </a> </li>
            </ul>
        </div>
        <div class="rcrumbs" id="breadcrumbs">
            <ul id="bread"></ul>
        </div>

        <a href="/" class="logo">
            <img src="/img/logo/logo.png" alt="Logo El Corral" />
        </a>
    </header>
    <input id="title" name="title" type="hidden" value="{{isset($title) ? $title : "El Corral &middot; Hamburguesas, Malteadas y Papitas"}}">
    <input type="hidden" id="breadcrumb_input" name="breadcrumb_input" value="{{isset($breadcrumb) ? $breadcrumb : ''}}">
    @yield('content')

{{--     <div  class="main main-ball" tabindex="1">
        <div class="cir">
            <a href="tel:(031)6543354" target="_blank" class="main main-item main-1">Llámanos</a>
            <a href="https://pideenlinea.elcorral.com/?source=pagElCorral" target="_blank" class="main main-item main-2">Pide en linea</a>
            <a href="/hablanos" target="" class="main main-item main-3">Servicio al cliente</a>
        </div>
    </div> --}}
<a class="ecommerce-fly" href="https://pideenlinea.elcorral.com/?source=pagElCorral" target="_blank" onclick="gtag('event', 'Clic', {'event_category': 'Mosca domicilios ', 'event_label': 'Mosca domicilios'});">
    <img src="/img/logo/Mosca-DomiciliosV2.png" alt="Domicilios El Corral" width="120px" style="z-index: 2">
</a>
    <!-- The slideshow -->
    <ul id="slideshow" data-speed="6000">
            <li>
                <img data-src="/img/slideshow/demo1.jpg" {{-- src="/img/slideshow/demo1.jpg" --}} alt="slideshow image" class="lazyload"/>
            </li>
            <li>
                <img data-src="/img/slideshow/demo2.jpg" {{-- src="/img/slideshow/demo2.jpg" --}} alt="slideshow image" class="lazyload"/>
            </li>
            <li>
                <img data-src="/img/slideshow/demo3.jpg" {{-- src="/img/slideshow/demo3.jpg" --}} alt="slideshow image" class="lazyload"/>
            </li>
            <li>
                <img data-src="/img/slideshow/demo4.jpg" {{-- src="/img/slideshow/demo4.jpg" --}} alt="slideshow image" class="lazyload"/>
            </li>
            <li>
                <img data-src="/img/slideshow/demo5.jpg" {{-- src="/img/slideshow/demo5.jpg" --}} alt="slideshow image" class="lazyload"/>
            </li>
            <li>
                <img data-src="/img/slideshow/demo6.jpg" {{-- src="/img/slideshow/demo6.jpg" --}} alt="slideshow image" class="lazyload"/>
            </li>
            <li>
                <img data-src="/img/slideshow/demo7.jpg" {{-- src="/img/slideshow/demo7.jpg" --}} alt="slideshow image" class="lazyload"/>
            </li>
        </ul>
        <!-- end of slideshow -->
        <div id="cajacookies">
            <p style="text-align: center">
                Éste sitio web usa cookies, si permanece aquí acepta su uso.
                Puede leer más sobre el uso de cookies en nuestra <a href="/pdf/Politica-de-Tratamiento-de-Datos-Personales.pdf"><strong>Política de Privacidad</b></a>.
                <button onclick="aceptarCookies()" class="badge-rounded"> Aceptar y cerrar</button>
            </p>
            <p>
            </p>
        </div>
    <script type="text/javascript" src="/js/libs/jquery-2.2.3.js"></script>
    <script>function compruebaAceptaCookies() {if(localStorage.aceptaCookies == 'true'){cajacookies.style.display = 'none';}}function aceptarCookies() {localStorage.aceptaCookies = 'true';cajacookies.style.display = 'none';}$(document).ready(function () {compruebaAceptaCookies();});</script>
    <script type="text/javascript" src="/js/lazysizes.min.js"></script>
    <script type="text/javascript" src="/js/custom/slider.js"></script>
    {{-- <script type="text/javascript" src="/js/libs/history.js"></script> --}}
    <script type="text/javascript" src="/js/libs/modernizr.js"></script>
    <script type="text/javascript" src="/js/libs/page.js"></script>
    {{-- <script type="text/javascript" src="/js/libs/backgroundvideo/backgroundvideo.js"></script> --}}
{{--     <script type="text/javascript" src="/js/libs/photoswipe/photoswipe.js"></script>
    <script type="text/javascript" src="/js/libs/photoswipe/photoswipe-ui-default.js"></script> --}}
    <script type="text/javascript" src="/js/libs/device.js"></script>
    {{-- <script type="text/javascript" src="/js/libs/remodal/remodal.js"></script> --}}
    <script type="text/javascript" src="/js/libs/jquery.rcrumbs.min.js"></script>
    <script type="text/javascript" src="/js/custom/slidenavigation.js"></script>
    <script type="text/javascript" src="/js/app.js"></script>
    <script type="text/javascript" src="/js/custom/legals.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAVDFX-wWsY5Dj--rthcUooMBoFsqsnlps&libraries=geometry"></script>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TLP8D7T"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
</body>

</html>