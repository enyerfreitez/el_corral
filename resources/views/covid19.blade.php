@php
    $description = "Encuentra en El Corral más cercano hamburguesas, perros calientes, wraps, papas a la francesa y malteadas para pedir a domicilio o pasar a recogerlos.";
    $title = "Horarios de restaurantes | El Corral: La receta original";
@endphp
@extends('layouts.main')
@section('content')
<div id="wrapper">
    <main>
        <div class="detail full">
            <section>
                <h1>Horarios y Servicios</h1>
            </section>
        </div>
        <!-- main content -->
        <div class="content-wrap full-width">
            <section>
                <div>
                    <div class="grid gallery hidden">
                        <div class="item">
                            <figure class="has-overlay">
                                <img src="/img/covid19/infografia-apertura-restaurantes-el-corral.jpg" alt="Infografía apertura restaurantes El Corral" title="Infografía apertura restaurantes El Corral"  class="lazyload">
                            </figure>
                        </div>
                        <div class="sizer"></div>
                        <div class="item">
                            <figure class="has-overlay">
                                <div >
                                    <div class="searchBlock input-covid" style="padding:5px;text-align:center;">
                                        <h2 class="covid">Selecciona tu ciudad:</h2>
                                        <select name="cities" id="cities_covid">
                                            <option  selected value="all">Vista General</option>
                                            @foreach ($cities as $item)
                                            <option value="{{$item->city}}">{{$item->city}}</option>
                                            @endforeach
                                        </select>
                                        <br>
                                        <label for="takeout" class="covid" onclick="gtag('event', 'Clic', {'event_category': 'Filtros contingencia ', 'event_label': 'Filtro takeout'});"><input type="checkbox" id="takeout" name="takeout">&nbsp;Pasa, pide y lleva</label><br>
                                        <label for="rappi" class="covid" onclick="gtag('event', 'Clic', {'event_category': 'Filtros contingencia ', 'event_label': 'Filtro Rappi'});"><input type="checkbox" id="rappi" name="rappi">&nbsp;Programa tu pedido y recoge</label> <br>
                                        <label for="own_delivery" class="covid" onclick="gtag('event', 'Clic', {'event_category': 'Filtros contingencia ', 'event_label': 'Filtro Domicilios propios'});"><input type="checkbox" id="own_delivery" name="own_delivery">&nbsp;Domicilio en línea o llámanos</label><br>
                                        <label for="reopening" class="covid" onclick="gtag('event', 'Clic', {'event_category': 'Filtros contingencia ', 'event_label': 'Filtro Comedor abierto'});"><input type="checkbox" id="reopening" name="reopening" >&nbsp;Comedor Abierto</label>
                                    </div>
                                </div>
                            </figure>
                            <figure>
                                <table id="pos_table" class="invisible">
                                    <thead>
                                        <th>Restaurante</th>
                                        <th>Dirección</th>
                                        <th>Ciudad</th>
                                        <th>Pasa, pide y lleva</th>
                                        <th>Programa tu pedido y recoge</th>
                                        <th>Domicilio en línea o llámanos</th>
                                        <th>Comedor Abierto</th>
                                        <th>Horario restaurante</th>
                                    </thead>
                                    <tbody id="pos_table_content">
                                        @foreach ($pdv as $pos)
                                        <tr id="{{$pos->id}}">
                                            <td data-label='Restaurante'>{{$pos->pdv}}</td>
                                            <td data-label='Dirección'>{{$pos->address}}, {{$pos->urb}}</td>
                                            <td data-label='Ciudad'>{{$pos->city}}</td>     
                                            <td data-label='Pasa, pide y lleva'>{{$pos->pick_able ? "Si": "No"}}</td>
                                            <td data-label='Rappi'>{{$pos->rappi ? "Si": "No"}}</td>
                                            <td data-label='Domicilio Propio'>{{$pos->own_delivery ? "Si": "No"}}</td>
                                            <td data-label='Comedor Abierto'>{{$pos->dinning_room ? "Plazoleta": "Comedor"}}<br>{{$pos->reopening ? "Abiert": "Cerrad"}}{{$pos->dinning_room ? "a": "o"}} {{$pos->reopening ? $pos->open_time : ""}}</td>
                                            <td data-label='Horario restaurante'>{{$pos->pick_time}}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </figure>
                        </div>
                    </div>
                </div>     
            </section>
        </div>
        <script type="text/javascript" src="/js/libs/jquery-2.2.3.js"></script>
        <script type="text/javascript" src="/js/custom/covid.js"></script>
        @include('layouts.footer')
        </div>
    </main>
    <!-- end of main content -->
</div>
@endsection