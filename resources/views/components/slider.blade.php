<div class="slider-container" id="slider-container" style="background-image: url({{ $slides->first()->img_uri }})">
    <div class="__control">
        <div class="__control-line"></div>
        <div class="__control-line"></div>
    </div>
    <div class="__control __control--right m--right" id="right_line">      
        <div class="__control-line"></div>
        <div class="__control-line"></div>
    </div>
    @foreach ($slides as $slide)
    <div class="__content" >
    {{-- <div class="__content __content--active" > --}}
        <h4>{!! $slide->copy ? nl2br(e($slide->copy)) : '' !!}</h4>
        <h3>{!!  nl2br(e($slide->title)) !!}</h3>
        <span style="display:none">{{$slide->img_uri}}</span>
        <p></p>
        @if ($slide->uri)
        <a href="{{ $slide->uri}}" class="btn-slide" target="__blank">
            {{ $slide->button_text != null ? $slide->button_text : "Conoce más" }}
        </a>
        @endif
    </div>
    @endforeach
</div>