<ul class="nav nav-tabs">
        <li class="active"><a class="tabable" href="#rone" data-toggle="tab"> Lista de Restaurantes</a></li>
        <li><a class="tabable" href="#rtwo" data-toggle="tab"> Registrar Restaurante</a></li>
        <li><a class="tabable" href="#rthree" data-toggle="tab"> Cambiar Banner</a></li>
      </ul>
      <br>
<div class="tab-content">
    <div class="tab-pane active" id="rone">
        <h1>Listado de Restaurantes</h1>
        <table id="example" class="table" style="width:100%">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>C.C</th>
                    <th>Nombre</th>
                    <th>Dirección</th>
                    <th>Ciudad</th>
                    <th>Horario</th>
                    <th>Ubicación</th>
                    <th>Servicios</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($restaurants as $restaurant)
                <tr>
                    <td>{{ $restaurant->id }}</td>
                    <td>{{ $restaurant->cost_center }}</td>
                    <td>{{ $restaurant->location_name }}</td>
                    <td>{{ $restaurant->address }}</td>
                    <td>{{ $restaurant->city }}</td>
                    <td>{{ $restaurant->schedule }}</td>
                    <td>
                        <ul>
                            <li>Lat: {{ $restaurant->lat }}</li>
                            <li>Lng: {{ $restaurant->lng }}</li>
                        </ul>
                    </td>
                    <td>
                        <ul>
                            <li style="{{ !$restaurant->wifi_zone ? 'display:none' : '' }}">Wifi</li>
                            <li style="{{ !$restaurant->pet_friendly ? 'display:none' : '' }}">Pet Friendly</li>
                            <li style="{{ !$restaurant->kid_zone ? 'display:none' : '' }}">Zona Niños</li>
                            <li style="{{ !$restaurant->parking ? 'display:none' : '' }}">Parqueadero</li>
                            <li style="{{ !$restaurant->valet_parking ? 'display:none' : '' }}">Valet Parking</li> 
                        </ul>
                    </td>
                    <td>
                        <div class="btn-group-vertical">
                            <a href="/adm/restaurants/edit?id={{$restaurant->id}}" class="btn btn-warning">Editar</a>
                            <form action="/adm/restaurants/delete" method="POST" onsubmit="return confirm('¿Estas seguro de realizar esta acción?');">
                                {{ csrf_field() }}
                                <input type="hidden" name="restaurant_id" value="{{ $restaurant->id }}">
                                <button class="btn btn-danger" style="width: auto;">
                                    Eliminar
                                </button>
                            </form>
                        </div>
                    </td>
                </tr>
                @endforeach
            </tbody>
            <tfoot>
                <tr>
                    <th>ID</th>
                    <th>C.C</th>
                    <th>Nombre</th>
                    <th>Dirección</th>
                    <th>Ciudad</th>
                    <th>Horario</th>
                    <th>Ubicación</th>
                    <th>Servicios</th>
                    <th>Acciones</th>
                </tr>
            </tfoot>
        </table>
    </div>




    <div class="tab-pane" id="rtwo">
        <h1>Registro de Restaurante</h1>
        <form action="/adm/restaurants/handle" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="redirection_url"> Centro de Costo</label>
                <input type="text" name="cost_center" id="cost_center" required>
            </div>
            <div class="form-group">
                <label for="redirection_url"> Nombre del Restaurante</label>
                <input type="text" name="location_name" id="location_name" required>
            </div>
            <div class="form-group">
                <label for="redirection_url"> Ciudad</label>
                <input type="text" name="city" id="city" required>
            </div>
            <div class="form-group">
                <label for="redirection_url"> Dirección</label>
                <input type="text" name="address" id="address" required>
            </div>
            <div class="form-group">
                <label for="redirection_url"> Latitud</label>
                <input type="text" name="lat" id="lat" required>
            </div>
            <div class="form-group">
                <label for="redirection_url"> Longitud</label>
                <input type="text" name="lng" id="lng" required>
            </div>
            <div class="form-group">
                <label for="redirection_url"> Horario</label>
                <input type="text" name="schedule" id="schedule" required>
            </div>
            <div class="form-group">
                <label for="redirection_url"> Wifi</label>
                <input type="checkbox" name="wifi_zone" id="wifi_zone">
            </div>
            <div class="form-group">
                <label for="redirection_url"> Pet Friendly</label>
                <input type="checkbox" name="pet_friendly" id="pet_friendly">
            </div>
            <div class="form-group">
                <label for="redirection_url"> Kid Zone</label>
                <input type="checkbox" name="kid_zone" id="kid_zone">
            </div>
            <div class="form-group">
                <label for="redirection_url"> Parqueadero</label>
                <input type="checkbox" name="parking" id="parking">
            </div>
            <div class="form-group">
                <label for="redirection_url"> Valet Parking</label>
                <input type="checkbox" name="valet_parking" id="valet_parking">
            </div>
            <button type="submit" class="submit" style="width: auto;">Registrar</button>
        </form>
    </div>
    <div class="tab-pane" id="rthree">
        <h1>Cambiar Banner de Restaurantes</h1>
        <p>Recuerda que las dimensiones del banner para el new deben ser de 1982 x 669px.</p>
        <form action="/adm/restaurants/banner" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="img_uri"> Imagen del banner</label>
                <p>Imagen Actual:</p>
                <img src="{{$banner_restaurant->img_uri}}" alt="" width="200px">
                <p>Recuerda que las dimensiones del banner para el new deben ser de 1982 x 669px.</p>
                <input type="file" name="img_uri" id="img_uri">
            </div>
            <div class="form-group">
                <label for="img_uri"> Texto Alternativo (obligatorio)</label>
                <input type="text" name="copy" id="copy" value="{{$banner_restaurant->copy}}" required>
            </div>
            <button type="submit" class="submit" style="width: auto;">Registrar</button>
        </form>
    </div>
</div>