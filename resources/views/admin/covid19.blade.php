@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="tabbable">
                        <ul class="nav nav-tabs">
                          <li class="active"><a href="#one" data-toggle="tab"> Listado</a></li>
                          <li><a href="#two" data-toggle="tab">Registro</a></li>
                        </ul>
                        <div class="tab-content">
                          <div class="tab-pane active" id="one">
                            <h1>Listado de Restaurantes en contingencia</h1>
                            <table id="example" class="table" style="width:100%">
                                <thead>
                                    <th>Restaurante</th>
                                    <th>Dirección</th>
                                    <th>Ciudad</th>
                                    <th>Teléfono</th>
                                    <th>Barrio</th>
                                    <th>Horario</th>
                                    <th>Pasa, pide y lleva</th>
                                    <th>Programa tu pedido y recoge</th>
                                    <th>Domicilio en línea o llámanos</th>
                                    <th>Reapertura</th>
                                    <th>Tipo Comedor</th>
                                    <th>Acciones</th>
                                </thead>
                                <tbody id="pos_table_content">
                                    @foreach ($contingencia as $pos)
                                    <tr id="{{$pos->id}}">
                                        <td data-label='Restaurante'>{{$pos->pdv}}</td>
                                        <td data-label='Dirección'>{{$pos->address}}</td>
                                        <td data-label='Ciudad'>{{$pos->city}}</td>
                                        <td data-label='Teléfono'>{{$pos->phone}}</td>
                                        <td data-label='Barrio'>{{$pos->urb}}</td>
                                        <td data-label='Horario'>{{$pos->pick_time}}</td>
                                        <td data-label='Para Llevar'>{{$pos->pick_able ? "Si": "No"}}</td>
                                        <td data-label='Rappi'>{{$pos->rappi ? "Si": "No"}}</td>
                                        <td data-label='Domicilio Propio'>{{$pos->own_delivery ? "Si": "No"}}</td>
                                        <td data-label='Reapertura'>{{$pos->reopening ? "Si": "No"}}</td>
                                        <td data-label='Reapertura'>{{$pos->dinning_room ? "Plazoleta": "Comedor"}}</td>
                                        <td data-label=''>
                                            <div class="btn-group-vertical">
                                                <a href="/adm/contingencia/edit?cont_id={{$pos->id}}" class="btn btn-warning">Editar</a>
                                                <a href="/adm/contingencia/enable?cont_id={{$pos->id}}" class="btn btn-primary">{{$pos->active == true ? "Desactivar" : "Activar"}}</a>
                                                <form action="/adm/contingencia/delete" method="POST" onsubmit="return confirm('¿Estas seguro de realizar esta acción?');">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="cont_id" value="{{ $pos->id }}">
                                                    <button class="btn btn-danger" style="width: auto;">
                                                        Eliminar
                                                    </button>
                                                </form>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <th>Restaurante</th>
                                    <th>Dirección</th>
                                    <th>Ciudad</th>
                                    <th>Teléfono</th>
                                    <th>Barrio</th>
                                    <th>Horario</th>
                                    <th>Pasa, pide y lleva</th>
                                    <th>Programa tu pedido y recoge</th>
                                    <th>Domicilio en línea o llámanos</th>
                                    <th>Reapertura</th>
                                    <th>Tipo Comedor</th>
                                    <th>Acciones</th>
                                </tfoot>
                            </table>
                          </div>
                          <div class="tab-pane" id="two">
                            <h1>Registro de Restaurante en Contingencia</h1>
                            <form action="/adm/contingencia/handle" method="POST" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="pdv"> Restaurante</label>
                                    <input type="text" name="pdv" id="pdv" required>
                                </div>
                                <div class="form-group">
                                    <label for="address"> Dirección</label>
                                    <input type="text" name="address" id="address" required>
                                </div>
                                <div class="form-group">
                                    <label for="city"> Ciudad</label>
                                    <input type="text" name="city" id="city" required>
                                </div>
                                <div class="form-group">
                                    <label for="phone"> Teléfono</label>
                                    <input type="text" name="phone" id="phone" required>
                                </div>
                                <div class="form-group">
                                    <label for="urb"> Barrio</label>
                                    <input type="text" name="urb" id="urb" required>
                                </div>
                                <div class="form-group">
                                    <label for="pick_time"> Horario</label>
                                    <input type="text" name="pick_time" id="pick_time" required>
                                </div>
                                <div class="form-group">
                                    <label for="pick_time"> Pasa, pide y lleva</label>
                                    <input type="checkbox" name="pick_able" id="pick_able">
                                </div>
                                <div class="form-group">
                                    <label for="rappi"> Programa tu pedido y recoge</label>
                                    <input type="checkbox" name="rappi" id="rappi">
                                </div>
                                <div class="form-group">
                                    <label for="own_delivery"> Domicilio en línea o llámanos</label>
                                    <input type="checkbox" name="own_delivery" id="own_delivery">
                                </div>
                                <div class="form-group">
                                    <label for="reopening">Reapertura</label>
                                    <input type="checkbox" name="reopening" id="reopening">
                                </div>
                                <div class="form-group">
                                    <label>Tipo de comedor</label>
                                    <input type="radio" name="dinning_room" value="0" required>&nbsp;Comedor
                                    <input type="radio" name="dinning_room" value="1" required>&nbsp;Plazoleta
                                </div>
                                <div class="form-group">
                                    <label for="open_time">Datos de apertura</label>
                                    <input type="text" name="open_time" id="open_time" required>
                                </div>
                                <button type="submit" class="submit" style="width: auto;">Registrar</button>
                            </form>
                          </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection