@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Editar Slide</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <h1>Actualización de Slide</h1>
        <p>Recuerda que las dimensiones del banner para el slide deben ser de 1350 x 570px.</p>
        <form action="/adm/slides/handle" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            <input type="hidden" name="slide_id" value="{{$slide->id}}">
            <div class="form-group">
                <label for="title"> Título del Slide</label>
                <textarea type="text" name="title" id="title" required>{{$slide->title}}</textarea>
            </div>
            <div class="form-group">
                <label for="copy"> Copy</label>
                <textarea type="text" name="copy" id="copy" >{{$slide->copy}}</textarea>
            </div>
            <div class="form-group">
                <label for="button_text"> Texto del Boton</label>
                <input type="text" name="button_text" id="button_text" value="{{$slide->button_text}}">
            </div>
            <div class="form-group">
                <label for="redirection_url"> URL de redirección del banner (opcional)</label>
            <input type="text" name="uri" value="{{$slide->uri}}">
            </div>
            <div class="form-group">
                <label for="redirection_url"> Imagen del Slide (obligatorio)</label>
                <p>Imagen Actual:</p>
            <img src="{{$slide->img_uri}}" alt="" width="300">
                <p>Recuerda que las dimensiones del banner para el slide deben ser de 1980 x 913px.</p>
                <input type="file" name="banner" id="banner">
            </div>
            <button type="submit" class="submit" style="width: auto;">Registrar</button>
        </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection