@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Editar Lo Nuevo: {{ $new->title }}</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="tabbable">
                        <ul class="nav nav-tabs">
                          <li class="active"><a href="#one" data-toggle="tab"> Actualizar Lo Nuevo</a></li>
                          <li><a href="#two" data-toggle="tab"> Editar Contenido</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="one">
                                <h1>Actualización de Lo Nuevo</h1>
                                <p>Recuerda que las dimensiones del banner para "lo nuevo" deben ser de 1350 x 570px.</p>
                                <form action="/adm/news/handle" method="POST" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                <input type="hidden" name="new_id" value="{{$new->id}}">
                                    <div class="form-group">
                                        <label for="redirection_url"> Título de Lo nuevo</label>
                                        <input type="text" name="title" id="title" value="{{$new->title}}" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="meta_title">Meta Título</label>
                                        <input type="text" name="meta_title" id="meta_title" value="{{$new->meta_title}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="redirection_url"> Descripción (Metadescripción)</label>
                                        <input type="text" name="description" id="description" value="{{$new->description}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="custom_url"> URL de Redirección Personalizada</label>
                                        <input type="text" name="custom_url" id="custom_url" value="{{$new->custom_url}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="button_text"> Texto del Boton</label>
                                        <input type="text" name="button_text" id="button_text" value="{{$new->button_text}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="redirection_url"> Imagen Lo Nuevo (obligatorio)</label>
                                        <p>Imagen Actual:</p>
                                    <img src="{{$new->banner_img}}" alt="" width="300">
                                        <p>Recuerda que las dimensiones del banner para lo nuevo deben ser de 1980 x 913px.</p>
                                        <input type="file" name="banner_img" id="banner">
                                    </div>
                                    <div class="form-group">
                                        <label for="redirection_url"> Logo de Campaña (obligatorio)</label>
                                        <p>Imagen Actual:</p>
                                    <img src="{{$new->campaign_tag_img}}" alt="" width="300">
                                        <p>Recuerda que las dimensiones del banner para lo nuevo deben ser de 500 x 250px. En PNG sin fondo.</p>
                                        <input type="file" name="campaign_tag_img" id="campaign_tag_img">
                                    </div>
                                    <div class="form-group">
                                        <label for="content_entry"> Contenido interno (opcional)</label>
                                        <textarea name="content_entry" id="contentNews" cols="100" rows="20">{{$new->content_entry}}</textarea>
                                    </div>
                                    <button type="submit" class="submit" style="width: auto;">Actualizar</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection