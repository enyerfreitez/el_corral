@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Editar Producto</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <h1>Actualización de Producto</h1>
        <form action="/adm/products/handle" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            <input type="hidden" name="product_id" value="{{$product->id}}">
            <div class="form-group">
                <label for="name"> Nombre</label>
                <input type="text" name="name" id="name" required value="{{$product->name}}">
            </div>
            <div class="form-group">
                <label for="description"> Descripción</label>
                <textarea name="description" id="description">{{$product->description}}</textarea>
            </div>
            <div class="form-group">
                <label for="category"> Categoría</label>
                <select name="category" required>
                    @foreach ($categories as $cat)
                    <option value="{{$cat->id}}" {{$cat->id == $product->pcategory_id ? 'selected' : '' }}>{{$cat->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="subcategory"> Subcategoría (Opcional)</label>
                <input type="text" name="subcategory" value="{{$product->subcategory}}">
            </div>
            <div class="form-group">
                <label for="img_uri"> Imagen del producto (obligatorio)</label>
                <img src="{{$product->img_uri}}" alt="" width="300">
                <p>Recuerda que las dimensiones de la imagen para el producto deben ser de 657 x 445px.</p>
                <input type="file" name="img_uri" id="img_uri">
            </div>
            <button type="submit" class="submit" style="width: auto;">Actualizar</button>
        </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection