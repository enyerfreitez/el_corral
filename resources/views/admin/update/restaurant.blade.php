@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Editar Restaurante</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <h1>Actualización de Restaurante</h1>
        <form action="/adm/restaurants/handle" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            <input type="hidden" name="restaurant_id" value="{{$restaurant->id}}">
            <div class="form-group">
                <label for="redirection_url"> Centro de Costo</label>
                <input type="text" name="cost_center" id="cost_center" value="{{$restaurant->cost_center}}" required>
            </div>
            <div class="form-group">
                <label for="redirection_url"> Nombre del Restaurante</label>
                <input type="text" name="location_name" id="location_name" value="{{$restaurant->location_name}}" required>
            </div>
            <div class="form-group">
                <label for="redirection_url"> Ciudad</label>
                <input type="text" name="city" id="city" value="{{$restaurant->city}}" required>
            </div>
            <div class="form-group">
                <label for="redirection_url"> Dirección</label>
                <input type="text" name="address" id="address" value="{{$restaurant->address}}" required>
            </div>
            <div class="form-group">
                <label for="redirection_url"> Latitud</label>
                <input type="text" name="lat" id="lat" value="{{$restaurant->lat}}" required>
            </div>
            <div class="form-group">
                <label for="redirection_url"> Longitud</label>
                <input type="text" name="lng" id="lng" value="{{$restaurant->lng}}" required>
            </div>
            <div class="form-group">
                <label for="redirection_url"> Horario</label>
                <input type="text" name="schedule" id="schedule" value="{{$restaurant->schedule}}" required>
            </div>
            <div class="form-group">
                <label for="redirection_url"> Wifi</label>
                <input type="checkbox" name="wifi_zone" id="wifi_zone" {{$restaurant->wifi_zone ? 'checked' : ''}}>
            </div>
            <div class="form-group">
                <label for="redirection_url"> Pet Friendly</label>
                <input type="checkbox" name="pet_friendly" id="pet_friendly" {{$restaurant->pet_friendly ? 'checked' : ''}}>
            </div>
            <div class="form-group">
                <label for="redirection_url"> Kid Zone</label>
                <input type="checkbox" name="kid_zone" id="kid_zone" {{$restaurant->kid_zone ? 'checked' : ''}}>
            </div>
            <div class="form-group">
                <label for="redirection_url"> Parqueadero</label>
                <input type="checkbox" name="parking" id="parking" {{$restaurant->parking ? 'checked' : ''}}>
            </div>
            <div class="form-group">
                <label for="redirection_url"> Valet Parking</label>
                <input type="checkbox" name="valet_parking" id="valet_parking">
            </div>
            <button type="submit" class="submit" style="width: auto;">Actualizar</button>
        </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection