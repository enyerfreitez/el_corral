@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Editar Producto</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <h1>Actualización de Producto</h1>
                    <form action="/adm/categories/handle" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="hidden" name="category_id" value="{{$category->id}}">
                        <div class="form-group">
                            <label for="name"> Nombre</label>
                            <input type="text" name="name" id="name" required value="{{$category->name}}">
                        </div>
                        <div class="form-group">
                            <label for="meta_title"> Meta Título</label>
                            <input type="text" name="meta_title" id="meta_title" required value="{{$category->meta_title}}">
                        </div>
                        <div class="form-group">
                            <label for="description"> Descripción</label>
                            <textarea name="description" id="description" required>{{$category->description}}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="img_uri"> Imagen de la Categoría</label>
                            <p>Imagen actual:</p>
                            <img src="{{$category->img_uri}}" alt="" width="300">
                            <p>Recuerda que las dimensiones de la imagen para la Categoría deben ser de 1980 x 658px.</p>
                            <input type="file" name="img_uri" id="img_uri">
                        </div>
                        <button type="submit" class="submit" style="width: auto;">Registrar</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection