@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Editar Legal</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <h1>Actualización de Legal</h1>
                    <form action="/adm/legals/handle" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="hidden" name="legal_id" value="{{$legal->id}}">
                        <div class="form-group">
                            <label for="title"> Título</label>
                            <input type="text" name="title" id="title" required value="{{$legal->title}}">
                        </div>
                        <div class="form-group">
                            <label for="text"> Descripción</label><br>
                            <textarea name="text" id="textlegal" cols="100" rows="20">{{$legal->text}}</textarea>
                        </div>
                        <button type="submit" class="submit" style="width: auto;">Actualizar</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection