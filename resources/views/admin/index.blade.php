@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                    <div class="tabbable">
                        <ul class="nav nav-tabs">
                          <li class="active"><a href="#one" data-toggle="tab"> Slides</a></li>
                          <li><a href="#six" data-toggle="tab">Lo Nuevo</a></li>
                          <li><a href="#two" data-toggle="tab">Productos</a></li>
                          <li><a href="#three" data-toggle="tab">Categorías de Productos</a></li>
                          <li><a href="#four" data-toggle="tab">Restaurantes</a></li>
                          <li><a href="#five" data-toggle="tab">Legales</a></li>
                        </ul>
                        <div class="tab-content">
                          <div class="tab-pane active" id="one">
                            @include('admin.slides')
                          </div>
                          <div class="tab-pane" id="two">
                              @include('admin.products')
                          </div>
                          <div class="tab-pane" id="three">
                              @include('admin.category')
                          </div>
                          <div class="tab-pane" id="four">
                              @include('admin.restaurants')
                            </div>
                            <div class="tab-pane" id="six">
                                @include('admin.news')
                          </div>
                            <div class="tab-pane" id="five">
                                @include('admin.legals')
                          </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection