<ul class="nav nav-tabs">
        <li class="active"><a class="tabable" href="#lone" data-toggle="tab"> Lista de Legales</a></li>
        <li><a class="tabable" href="#ltwo" data-toggle="tab"> Registrar Legal</a></li>
      </ul>
      <br>
<div class="tab-content">
    <div class="tab-pane active" id="lone">
        <h1>Listado de Legales</h1>
        <table id="example" class="table" style="width:100%">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Título</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($legals as $legal)
                <tr>
                    <td>{{ $legal->id }}</td>
                    <td>{{ $legal->title }}</td>
                    <td>
                        <div class="btn-group-vertical">
                            <a href="/adm/legals/edit?id={{$legal->id}}" class="btn btn-warning">Editar</a>
                            <form action="/adm/legals/delete" method="POST" onsubmit="return confirm('¿Estas seguro de realizar esta acción?');">
                                {{ csrf_field() }}
                                <input type="hidden" name="legal_id" value="{{ $legal->id }}">
                                <button class="btn btn-danger" style="width: auto;">
                                    Eliminar
                                </button>
                            </form>
                        </div>
                    </td>
                </tr>
                @endforeach
            </tbody>
            <tfoot>
                <tr>
                    <th>ID</th>
                    <th>Título</th>
                    <th>Acciones</th>
                </tr>
            </tfoot>
        </table>
    </div>




    <div class="tab-pane" id="ltwo">
        <h1>Registro de Legal</h1>
        <p>Recuerda que las dimensiones de la imagen para el legalo deben ser de 657 x 445px.</p>
        <form action="/adm/legals/handle" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="title"> Título</label>
                <input type="text" name="title" id="title" required>
            </div>
            <div class="form-group">
                <label for="text"> Descripción</label><br>
                <textarea name="text" id="textlegal" cols="100" rows="20"></textarea>
            </div>
            <button type="submit" class="submit" style="width: auto;">Registrar</button>
        </form>
    </div>
</div>