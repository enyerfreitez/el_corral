<ul class="nav nav-tabs">
        <li class="active"><a class="tabable" href="#cone" data-toggle="tab"> Lista de Categorias</a></li>
        <li><a class="tabable" href="#ctwo" data-toggle="tab"> Registrar Categoria</a></li>
      </ul>
      <br>
<div class="tab-content">
    <div class="tab-pane active" id="cone">
        <h1>Listado de Categorias</h1>
        <p>Recuerda que puedes reordenar las categorias moviendo las filas</p>
        <table id="example" class="table" style="width:100%">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Nombre</th>
                    <th>Descripción</th>
                    <th>Imagen</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody class="row_position" id="ProductCategory">
                @foreach ($categories as $category)
                <tr id="{{ $category->id }}">
                    <td>{{ $category->id }}</td>
                    <td>{{ $category->name }}</td>
                    <td>{{ $category->description }}</td>
                    <td>
                        <img src="{{ $category->img_uri }}" width="200px">
                    </td>
                    <td>
                        <form action="/adm/categories/delete" method="POST" onsubmit="return confirm('¿Estas seguro de realizar esta acción?');">
                            <div class="btn-group-vertical">
                                <a href="/adm/categories/list/?id={{$category->id}}" class="btn btn-success">Reordenar</a>
                                <a href="/adm/categories/edit?id={{$category->id}}" class="btn btn-warning">Editar</a>
                                {{ csrf_field() }}
                                <input type="hidden" name="category_id" value="{{ $category->id }}">
                                <button class="btn btn-danger">
                                    Eliminar
                                </button>
                            </div>
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
            <tfoot>
                <tr>
                    <th>ID</th>
                    <th>Redirección</th>
                    <th>Imagen</th>
                    <th>Acciones</th>
                </tr>
            </tfoot>
        </table>
    </div>




    <div class="tab-pane" id="ctwo">
        <h1>Registro de Categoria</h1>
        <p>Recuerda que las dimensiones de la imagen para la categoría deben ser de 1980 x 658px.</p>
        <form action="/adm/categories/handle" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="name"> Nombre</label>
                <input type="text" name="name" id="name" required>
            </div>
            <div class="form-group">
                <label for="meta_title"> Meta Título</label>
                <input type="text" name="meta_title" id="meta_title" required>
            </div>
            <div class="form-group">
                <label for="description"> Descripción</label>
                <textarea name="description" id="description" required></textarea>
            </div>
            <div class="form-group">
                <label for="img_uri"> Imagen de la Categoría (obligatorio)</label>
                <p>Recuerda que las dimensiones de la imagen para la Categoría deben ser de 1980 x 658px.</p>
                <input type="file" name="img_uri" id="img_uri" required>
            </div>
            <button type="submit" class="submit" style="width: auto;">Registrar</button>
        </form>
    </div>
</div>