@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Editar Restaurante en contingencia</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <h1>Actualización de Restaurante</h1>
        <form action="/adm/contingencia/handle" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            <input type="hidden" name="cont_id" value="{{$cont->id}}">
            <div class="form-group">
                <label for="pdv"> Restaurante</label>
                <input type="text" name="pdv" id="pdv" required value="{{$cont->pdv}}">
            </div>
            <div class="form-group">
                <label for="address"> Dirección</label>
                <input type="text" name="address" id="address" required value="{{$cont->address}}">
            </div>
            <div class="form-group">
                <label for="city"> Ciudad</label>
                <input type="text" name="city" id="city" required value="{{$cont->city}}">
            </div>
            <div class="form-group">
                <label for="phone"> Teléfono</label>
                <input type="text" name="phone" id="phone" required value="{{$cont->phone}}">
            </div>
            <div class="form-group">
                <label for="urb"> Barrio</label>
                <input type="text" name="urb" id="urb" required value="{{$cont->urb}}">
            </div>
            <div class="form-group">
                <label for="pick_time"> Horario</label>
                <input type="text" name="pick_time" id="pick_time" required value="{{$cont->pick_time}}">
            </div>
            <div class="form-group">
                <label for="pick_time"> Pasa, pide y lleva</label>
                <input type="checkbox" name="pick_able" id="pick_able" {{$cont->pick_able ? 'checked' : ''}}>
            </div>
            <div class="form-group">
                <label for="rappi"> Programa tu pedido y recoge</label>
                <input type="checkbox" name="rappi" id="rappi" {{$cont->rappi ? 'checked' : ''}}>
            </div>
            <div class="form-group">
                <label for="own_delivery">  Domicilio en línea o llámanos</label>
                <input type="checkbox" name="own_delivery" id="own_delivery" {{$cont->own_delivery ? 'checked' : ''}}>
            </div>
            <div class="form-group">
                <label for="own_delivery"> Reapertura</label>
                <input type="checkbox" name="reopening" id="reopening" {{$cont->reopening ? 'checked' : ''}}>
            </div>
            <div class="form-group">
                <label>Tipo de comedor</label>
                <input type="radio" name="dinning_room" value="0" {{!$cont->dinning_room ? 'checked="checked"' : ''}} required>&nbsp;Comedor
                <input type="radio" name="dinning_room" value="1" {{$cont->dinning_room ? 'checked="checked"' : ''}} required>&nbsp;Plazoleta
            </div>
            <div class="form-group">
                <label for="open_time">Datos de apertura</label>
                <input type="text" name="open_time" id="open_time" required value="{{$cont->open_time}}">
            </div>
            <button type="submit" class="submit" style="width: auto;">Actualizar</button>
        </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection