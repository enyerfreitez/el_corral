<ul class="nav nav-tabs">
        <li class="active"><a class="tabable" href="#sone" data-toggle="tab"> Lista de Slides</a></li>
        <li><a class="tabable" href="#stwo" data-toggle="tab"> Registrar Slide</a></li>
      </ul>
      <br>
<div class="tab-content">
    <div class="tab-pane active" id="sone">
        <h1>Listado de Slides</h1>
        <p>Recuerda que puedes reordenar los slides moviendo las filas</p>
        <table id="slides" class="table" style="width:100%">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Título</th>
                    <th>Copy</th>
                    <th>Texto del Boton</th>
                    <th>Redirección</th>
                    <th>Imagen</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody class="row_position" id="Slide">
                @foreach ($slides as $slide)
                <tr id="{{ $slide->id }}">
                    <td>{{ $slide->id }}</td>
                    <td>{{ $slide->title }}</td>
                    <td>{{ $slide->copy }}</td>
                    <td>{{ $slide->button_text }}</td>
                    <td>{{ $slide->uri }}</td>
                    <td>
                        <img src="{{ $slide->img_uri }}" width="200px">
                    </td>
                    <td>
                        <div class="btn-group-vertical">
                                <a href="/adm/slides/edit?id={{$slide->id}}" class="btn btn-warning">Editar</a>
                            <form action="/adm/slides/delete" method="POST" onsubmit="return confirm('¿Estas seguro de realizar esta acción?');">
                                {{ csrf_field() }}
                                <input type="hidden" name="slide_id" value="{{ $slide->id }}">
                                <button class="btn btn-danger" style="width: auto;">
                                    Eliminar
                                </button>
                            </form>
                        </div>
                    </td>
                </tr>
                @endforeach
            </tbody>
            <tfoot>
                <tr>
                    <th>ID</th>
                    <th>Título</th>
                    <th>Redirección</th>
                    <th>Imagen</th>
                    <th>Acciones</th>
                    
                </tr>
            </tfoot>
        </table>
    </div>



    <div class="tab-pane" id="stwo">
        <h1>Registro de Slide</h1>
        <p>Recuerda que las dimensiones del banner para el slide deben ser de 1980 x 913px.</p>
        <form action="/adm/slides/handle" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="title"> Título del Slide</label>
                <textarea type="text" name="title" id="title" required></textarea>
            </div>
            <div class="form-group">
                <label for="copy"> Copy</label>
                <textarea type="text" name="copy" id="copy"></textarea>
            </div>
            <div class="form-group">
                <label for="button_text"> Texto del Boton</label>
                <input type="text" name="button_text" id="button_text">
            </div>
            <div class="form-group">
                <label for="redirection_url"> URL de redirección del banner (opcional)</label>
                <input type="text" name="uri">
            </div>
            <div class="form-group">
                <label for="redirection_url"> Imagen del Slide (obligatorio)</label>
                <p>Recuerda que las dimensiones del banner para el slide deben ser de 1980 x 913px.</p>
                <input type="file" name="banner" id="banner" required>
            </div>
            <button type="submit" class="submit" style="width: auto;">Registrar</button>
        </form>
    </div>
</div>