<ul class="nav nav-tabs">
        <li class="active"><a class="tabable" href="#pone" data-toggle="tab"> Lista de Productos</a></li>
        <li><a class="tabable" href="#ptwo" data-toggle="tab"> Registrar Producto</a></li>
      </ul>
      <br>
<div class="tab-content">
    <div class="tab-pane active" id="pone">
        <h1>Listado de Productos</h1>
        <table id="example" class="table" style="width:100%">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Categoría</th>
                    <th>Subcategoría</th>
                    <th>Nombre</th>
                    <th>Descripción</th>
                    <th>Imagen</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($products as $product)
                <tr>
                    <td>{{ $product->id }}</td>
                    <td>{{ $product->productCategory ? $product->productCategory->name : null }}</td>
                    <td>{{ $product->subcategory }}</td>
                    <td>{{ $product->name }}</td>
                    <td>{{ $product->description }}</td>
                    <td>
                        <img src="{{ $product->img_uri }}" width="200px">
                    </td>
                    <td>
                        <div class="btn-group-vertical">
                            <a href="/adm/products/edit?id={{$product->id}}" class="btn btn-warning">Editar</a>
                            <form action="/adm/products/delete" method="POST" onsubmit="return confirm('¿Estas seguro de realizar esta acción?');">
                                {{ csrf_field() }}
                                <input type="hidden" name="product_id" value="{{ $product->id }}">
                                <button class="btn btn-danger" style="width: auto;">
                                    Eliminar
                                </button>
                            </form>
                        </div>
                    </td>
                </tr>
                @endforeach
            </tbody>
            <tfoot>
                <tr>
                    <th>ID</th>
                    <th>Categoría</th>
                    <th>Subcategoría</th>
                    <th>Nombre</th>
                    <th>Descripción</th>
                    <th>Imagen</th>
                    <th>Acciones</th>
                </tr>
            </tfoot>
        </table>
    </div>




    <div class="tab-pane" id="ptwo">
        <h1>Registro de Producto</h1>
        <p>Recuerda que las dimensiones de la imagen para el producto deben ser de 657 x 445px.</p>
        <form action="/adm/products/handle" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="name"> Nombre</label>
                <input type="text" name="name" id="name" required>
            </div>
            <div class="form-group">
                <label for="description"> Descripción</label>
                <textarea name="description" id="description"></textarea>
            </div>
            <div class="form-group">
                <label for="category"> Categoría</label>
                <select name="category" required>
                    <option selected hidden>Selecciona una Categoría</option>
                    @foreach ($categories as $cat)
                <option value="{{$cat->id}}">{{$cat->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="subcategory"> Subcategoría (Opcional)</label>
                <input type="text" name="subcategory">
            </div>
            <div class="form-group">
                <label for="img_uri"> Imagen del producto (obligatorio)</label>
                <p>Recuerda que las dimensiones de la imagen para el producto deben ser de 657 x 445px.</p>
                <input type="file" name="img_uri" id="img_uri" required>
            </div>
            <button type="submit" class="submit" style="width: auto;">Registrar</button>
        </form>
    </div>
</div>