@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
            <div class="panel-heading">Productos de categoría {{$cat->name}}</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <h1>Listado de Productos</h1>
                    <table id="example" class="table" style="width:100%">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Categoría</th>
                                <th>Subcategoría</th>
                                <th>Nombre</th>
                                <th>Descripción</th>
                                <th>Imagen</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody class="row_position" id="Product">
                            @foreach ($cat->products->sortBy('order') as $product)
                            <tr id="{{ $product->id }}">
                                <td>{{ $product->id }}</td>
                                <td>{{ $product->productCategory->name }}</td>
                                <td>{{ $product->subcategory }}</td>
                                <td>{{ $product->name }}</td>
                                <td>{{ $product->description }}</td>
                                <td>
                                    <img src="{{ $product->img_uri }}" width="200px">
                                </td>
                                <td>
                                    <div class="btn-group-vertical">
                                        <a href="/adm/products/edit?id={{$product->id}}" class="btn btn-warning">Editar</a>
                                        <form action="/adm/products/delete" method="POST" onsubmit="return confirm('¿Estas seguro de realizar esta acción?');">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="product_id" value="{{ $product->id }}">
                                            <button class="btn btn-danger" style="width: auto;">
                                                Eliminar
                                            </button>
                                        </form>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>ID</th>
                                <th>Categoría</th>
                                <th>Subcategoría</th>
                                <th>Nombre</th>
                                <th>Descripción</th>
                                <th>Imagen</th>
                                <th>Acciones</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection