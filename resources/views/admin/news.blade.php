<ul class="nav nav-tabs">
    <li class="active"><a class="tabable" href="#none" data-toggle="tab"> Lista de Lo Nuevo</a></li>
    <li><a class="tabable" href="#ntwo" data-toggle="tab"> Registrar Lo Nuevo</a></li>
</ul>
<br>
<div class="tab-content">
    <div class="tab-pane active" id="none">
        <h1>Listado de Lo Nuevo</h1>
        <p>Recuerda que puedes reordenar los items moviendo las filas</p>
        <table id="news" class="table" style="width:100%">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Título</th>
                    <th>Descripción</th>
                    <th>Banner</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody class="row_position" id="News">
                @foreach ($news as $new)
                <tr id="{{ $new->id }}">
                    <td>{{ $new->id }}</td>
                    <td>{{ $new->title }}</td>
                    <td>{{ $new->description }}</td>
                    <td>
                        <img src="{{ $new->banner_img }}" width="200px">
                    </td>
                    <td>
                        <div class="btn-group-vertical">
                            <a href="/adm/news/edit?id={{$new->id}}" class="btn btn-warning">Editar</a>
                            <form action="/adm/news/delete" method="POST" onsubmit="return confirm('¿Estas seguro de realizar esta acción?');">
                                {{ csrf_field() }}
                                <input type="hidden" name="new_id" value="{{ $new->id }}">
                                <button class="btn btn-danger" style="width: auto;">
                                    Eliminar
                                </button>
                            </form>
                        </div>
                    </td>
                </tr>
                @endforeach
            </tbody>
            <tfoot>
                <tr>
                    <th>ID</th>
                    <th>Título</th>
                    <th>Descripción</th>
                    <th>Banner</th>
                    <th>Acciones</th>
                </tr>
            </tfoot>
        </table>
    </div>



    <div class="tab-pane" id="ntwo">
        <h1>Registro de Lo Nuevo</h1>
        <p>Recuerda que las dimensiones del banner para el new deben ser de 1982 x 669px.</p>
        <form action="/adm/news/handle" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="redirection_url">Título</label>
                <input type="text" name="title" id="title" required>
            </div>
            <div class="form-group">
                <label for="meta_title">Meta Título</label>
                <input type="text" name="meta_title" id="meta_title">
            </div>
            <div class="form-group">
                <label for="description"> Descripción  (Metadescripción)</label>
                <input type="text" name="description" id="description" required>
            </div>
            <div class="form-group">
                <label for="custom_url"> URL de Redirección Personalizada</label>
                <input type="text" name="custom_url" id="custom_url">
            </div>
            <div class="form-group">
                <label for="button_text"> Texto del Boton</label>
                <input type="text" name="button_text" id="button_text" required>
            </div>
            <div class="form-group">
                <label for="banner_img"> Banner Principal (obligatorio)</label>
                <p>Recuerda que las dimensiones del banner para el new deben ser de 1982 x 669px.</p>
                <input type="file" name="banner_img" id="banner_img" required>
            </div>
            <div class="form-group">
                <label for="campaign_tag_img"> Logo de Campaña (obligatorio)</label>
                <p>Recuerda que las dimensiones del banner para el new deben ser de 500 x 250px. En PNG sin fondo.</p>
                <input type="file" name="campaign_tag_img" id="campaign_tag_img" required>
            </div>
            
            <hr>
            <div class="form-group">
                <label for="content_entry"> Añadir contenido interno (opcional)</label>
                <textarea name="content_entry" id="contentNews" cols="100" rows="20"></textarea>
            </div>

            <hr>
            <button type="submit" class="submit" style="width: auto;">Registrar</button>
        </form>
    </div>
</div>