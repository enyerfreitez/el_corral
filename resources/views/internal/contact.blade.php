@php
    $description = '¡Si tienes una sugerencia, contáctanos! Cada día trabajamos para ser mejores y tener las más deliciosas recetas en comida rápida a tu alcance!';
    $title = 'Háblanos y Contáctanos | El Corral:  La Receta Original';
    $schema = '
    <script type="application/ld+json"> 
    {
    "@context": "http://www.schema.org",
    "@type": "ContactPage",
    "name": "Háblanos",
    "description" : "'.$description.'",
    "alternateName": "Contacto",
    "url": "https://elcorral.com/hablanos"
    }
    </script>
    ';
    $breadcrumb = "HÁBLANOS";
@endphp
@extends('layouts.main')
@section('content')

<div id="wrapper">
    <main>
        <div class="detail full">
            <section>
                <h1>Háblanos</h1>
            </section>
        </div>
        <div class="content-wrap full-width">
            <section>
                <div>
                    <div class="grid gallery has-display">
{{--                         <div class="sizer"></div>
                        <div class="item">
                            <figure class="has-overlay">
                                <img src="/img/contact/Hablanos.jpg" alt="hablanos" >
                            </figure>
                        </div> --}}
                        <div class="sizer"></div>
                        <div class="item centered need-top-padding">
                            <figure>
                                <div class="two-cols-content contact">
{{--                                     <div>
                                        <form action="https://aplicaciones.alimentosalconsumidor.com/formularios/insertar/formulario-hec.php" method="post">
                                            <div style="hidden">
                                                <input type="hidden" value="on" name="acepto">
                                                <input type="hidden" value="HAMBURGUESAS EL CORRAL" name="description[Marca]">
                                                <input type="hidden" value="https://elcorral.com/" name="redirect_url">
                                                <input type="hidden" value="" name="description[sociedad]">
                                                <input type="hidden" value="" name="description[spc_id]">
                                                <input type="hidden" value="" name="description[tipo_formulario]">
                                                <input type="hidden" value="" name="description[direccion]">
                                                <input type="hidden" value="" name="fields[account_id]">
                                                <input type="hidden" value="" name="fields[assigned_user_id]">
                                                <input type="hidden" value="" name="fields[contact_id]">
                                                <input type="hidden" value="" name="fields[created_by]">
                                                <input type="hidden" value="" name="fields[modified_user_id]">
                                                <input type="hidden" value="" name="fields[sasa_abreviaturasociedad_c]">
                                                <input type="hidden" value="" name="fields[sasa_fuente_c]">
                                                <input type="hidden" value="" name="genero">
                                                <input type="hidden" value="WEB" name="fields[source]">
                                                <input type="hidden" value="New" name="fields[status]">
                                                <input type="hidden" value="" name="fields[team_id]">
                                                <input type="hidden" value="" name="fields[team_set_id]">
                                                <input type="hidden" value="" name="fields[type]">
                                                <input type="hidden" value="" name="fields[sasa_relacion_c]">
                                                <input type="hidden" value="" name="fields[sasa_tificacion_casos_cases_1sasa_tificacion_casos_ida]">
                                            </div>
                                            <h1>Cuéntanos sobre ti</h1>
                                            <input type="text" name="description[first_name]" id="name" placeholder="Nombres:" required>
                                            <input type="text" name="description[last_name]" id="lastname" placeholder="Apellidos:" required>
                                            <select class="" id="tipoID" name="description[tipoID]" required>
                                                    <option selected hidden>Tipo de documento:</option>
                                                    <option value="CC">Cédula de ciudadania</option>
                                                    <option value="NIT">NIT</option>
                                                    <option value="CE">Cédula de extranjería</option>
                                                    <option value="PAS">Pasaporte</option>
                                                    <option value="OTROS">Otros</option>
                                                  </select>                                            
                                            
                                            <input type="text" name="description[contact_cedula]" id="doc" placeholder="Número de documento:" required>
                                            
                                            <h1>¿Dónde te Contactamos?</h1>
                                            
                                            <input type="tel" name="description[numero_celular]" id="phone" placeholder="Celular:" required>
                                            <input type="email" name="description[correo_electronico]" id="email" placeholder="Correo Eletrónico:" required>
            
                                            <h1>Por favor, ingresa el nombre del punto de venta (si aplica)</h1>
                                            <input type="text" name="punto_de_venta" id="pdv" placeholder="Ingresa el punto de venta:">
                                            
                                            <h1>¿Cuál es el motivo de contacto?</h1>
                                            <input type="text" name="description[motivo_contacto]" id="reason" placeholder="Motivo de contacto:" required>
                                            <p class="justified text">
                                                Ten en cuenta que cualquier solicitud relacionada con Rappi debe realizarse directamente con ellos por su canal de soporte a través de la APP
                                                <br>
                                                Si deseas realizar un pedido en línea ingresa a:  <a href="https://pideenlinea.elcorral.com/FrmLocation.aspx#no-back-button" target="__blank">pideenlinea.elcorral.com/.</a>
                                            </p>
                                            <p class="text" style="text-align:center">
                                                <input type="checkbox" name="politica_de_datos" id="privacy" required>&nbsp;<a href="/pdf/Politica-de-Tratamiento-de-Datos-Personales.pdf" target="__blank">Política de Tratamiento de Datos</a></li>
                                            </p>
            
                                            <button type="submit" class="button">
                                                Contactar
                                            </button>
                                        </form>
                                    </div> --}}

                                    <div>
                                        <h2 >Para realizar tu pedido, conocer el estado de tu domicilio o hacer preguntas, quejas o sugerencias </h2>
                                        <a href="tel:0316543354">
                                            <img src="/img/contact/numero-telefono-el-corral-bogota.png" alt="Número de teléfono El Corral Bogotá" class="call-btn lazyload"  title="Número de teléfono El Corral Bogotá">
                                        </a>
                                        <br>
                                        <a href="tel:0312836999">
                                            <img src="/img/contact/numero-de-telefono-el-corral-medellin.png" alt="Número de teléfono El Corral Medellín" title="Número de teléfono El Corral Medellín"  class="lazyload call-btn">
                                        </a>
                                        <br>
                                        <a href="tel:3336222122">
                                            <img src="/img/contact/numero-telefono-el-corral-todo-el-pais.png" alt="Número de teléfono El Corral todo el país" title="Número de teléfono El Corral todo el país"  class="lazyload call-btn">
                                        </a>
                                        <br><br>
                                        <h2 >Si realizaste tu pedido por nuestra página web y necesitas ayuda escríbenos por Whatsapp al:</h2>
                                        <a target="__blank" href="https://api.whatsapp.com/send?phone=573209264190&text=%C2%A1Hola!%20Necesito%20ayuda%20con%20mi%20pedido:%20">
                                            <img src="/img/contact/numero-de-whatsapp-el-corral.png" alt="Número de whatsapp El Corral" title="Número de whatsapp El Corral"  class="lazyload call-btn">
                                        </a>
                                        <h2>También puedes contactarnos por <br> nuestras Redes Sociales </h2>
                                        <ul>
                                            <li>
                                                <a href="https://www.facebook.com/hamburguesaselcorral/" target="__blank" class="sprite fb" title="Facebook El Corral">
                                                </a>
                                            </li>
                                            <li>
                                                <a href="https://www.instagram.com/elcorraloficial/" target="__blank" class="sprite ig" title="Instagram El Corral">
                                                </a>
                                            </li>
                                            <li>
                                                <a href="https://twitter.com/elcorral_?lang=es" target="__blank" class="sprite tw" title="Twitter El Corral">
                                                </a>
                                            </li>
                                            <li>
                                                <a href="https://www.youtube.com/user/HamburguesasElCorral" target="__blank" class="sprite yt" title="Youtube El Corral">
                                                </a>
                                            </li>
                                            <li>
                                                <a href="https://vm.tiktok.com/KHGpdC/" target="__blank" class="sprite tt" title="TikTok El Corral">
                                                </a>
                                            </li>
                                        </ul>
                                        <p class=" subtitle">
                                            Aviso de Privacidad
                                        </p>
                                        <p class="justified text">
                                            Autorizo a Grupo Nutresa S.A. y a sus subordinadas, filiales y subsidiarias para tratar mis datos personales con la finalidad de 
                                            gestión de pedidos y hacerle seguimiento, llevar a cabo facturación de los productos o servicios, 
                                            realizar promociones o concursos, realizar actividades de mercadeo propias o de terceros, llevar a 
                                            cabo encuestas, análisis y estudios demercadeo, evaluar la calidad de productos y servicios, prestar 
                                            asistencia sobre ellos, atender PQR´S, contactarme en cualquier medio, incluso realizar transferencias 
                                            o transmisión de ellos de conformidad con la Política de Tratamiento de Datos Personales de Grupo Nutresa S.A.
                                            <br>
                                            <br>
                                            En cualquier momento, puedes conocer, actualizar y/o solicitar la rectificación de tus datos personales 
                                            dando Actualizar, o puedes ser eliminado nuestras bases dando Eliminar. También puedes formular solicitudes 
                                            a través de cualquiera de nuestros canales dispuestos para el efecto.
                                                
                                            
                                        </p>
                                    </div>
                                </div>
                            </figure>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        @include('layouts.footer')
        </div>
    </main>
    <!-- end of main content -->
</div>

@endsection