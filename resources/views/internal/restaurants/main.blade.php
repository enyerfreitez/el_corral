@php
    $description = "Encuentra tu restaurante Corral más cercano y disfruta de tus hamburguesas favoritas, perros calientes o wraps acompañadas de una malteada o papas fritas.";
    $title ='Restaurantes | El Corral: La Receta Original';
@endphp
@extends('layouts.main')
@section('content')

<div id="wrapper">
    <main>
        <div class="detail full">
            <section>
                <h1>Restaurantes</h1>
            </section>
        </div>
        <div class="content-wrap full-width">
            <section>
                <div>
                    <div class="grid gallery hidden">
                        <div class="item">
                            <figure class="has-overlay">
                                <a href="https://pideenlinea.elcorral.com/?source=pagElCorral" target="__blank">
                               <img src="{{ $banner_restaurant->img_uri }}"  alt="{{ $banner_restaurant->copy }}" class="lazyload"/>
                               </a>
                            </figure>
                        </div>
                        <div class="sizer"></div>
                        <div class="item">
                            <figure class="has-overlay">
                                <div class="two-cols-content map">
                                    <div class="searchBlock">
                                        <h2>Ciudad:</h2>
                                        <select name="cities" id="cities">
                                            <option  selected disabled hidden>Selecciona una Ciudad</option>
                                            @foreach ($cities as $city)
                                            <option value="{{ $city->city }}">{{ $city->city }}</option>
                                            @endforeach
                                        </select>
                                        <h2>Zona (Sólo Bogotá):</h2>
                                        <select name="zones" id="zones" disabled >
                                            <option  selected disabled hidden>Aplica sólo a Bogotá</option>
                                        </select>
                                        <p id="zoneDef"></p>
                                        
                                        <h2>Restaurantes:</h2>
                                        <select name="markers" id="markers">
                                            <option  selected disabled hidden id="Fone">Selecciona un Restaurante</option>
                                        </select>
                                        <div class="info-box" id="box">
                                            <div class="need-top-padding left-pad">
                                                <h3 id="name"></h3>
                                                <h3 id="address"></h3>
                                                <h3 id="schedule"></h3>
                                            </div>
                                            <ul id="services" style="display:none">
                                                <li id="pet_friendly"></li>
                                                <li id="kid_zone"></li>
                                                <li id="parking"></li>
                                                <li id="valet_parking"></li>
                                            </ul> 
                                        </div>
                                    </div>
                                    <div style="height:80vh">
                                            <div id="map" style="height:inherit;width:100%"></div>
                                            <script type="text/javascript" src="/js/libs/jquery-2.2.3.js"></script>
                                            <script type="text/javascript" src="/js/custom/maps.js"></script>
                                    </div>

                                    </div>
                            </figure>
                        </div>
                    </div>
                </div>     
            </section>
        </div>
        @include('layouts.footer')
        </div>
    </main>
    <!-- end of main content -->
</div>

@endsection