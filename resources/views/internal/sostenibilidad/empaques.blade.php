@php
    $title = 'Empaques con desarrollo sostenible | El Corral';
    $description = '¿Sabías que el empaque en el que comes tus papitas o tomas tu bebida de El Corral es biodegradable? ¡Conoce nuestro desarrollo sostenible!';
    $breadcrumb = "LO DE AFUERA TAMBIÉN IMPORTA";
@endphp
@extends('layouts.main')
@section('content')

<div id="wrapper">
    <main>
        <!-- main content -->
        <div class="detail full">
            <section class="full-s">
                <h1>Lo de afuera también importa</h1>
            </section>
        </div>
        <div class="content-wrap full-width">
            <section>
                <div>
                    <div class="grid gallery hidden">
                        {{--<div class="sizer"></div>
                        <div class="item">
                            <a href="sosteniblidad/lo-de-afuera-tambien-importa">
                                <figure class="has-overlay">
                                    <img src="/img/sostenibilidad/empaques/banner.jpg" />
                                </figure>
                            </a>
                        </div>--}}
                        <div class="sizer"></div>
                        <div class="item">
                            <figure class="has-overlay">
                                    {{--<iframe style="width:100%!important;height:500px" src="https://player.vimeo.com/video/349680099?autoplay=1&background=1" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>--}}
                                    <iframe style="width:100%!important;height:500px" src="https://www.youtube.com/embed/ws7SxfjE4-Q?rel=0&autoplay=0&showinfo=0&controls=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </figure>
                        </div>
                    </div>
                </div>     
            </section>
        </div>
        @include('layouts.footer')
        </div>
    </main>
    <!-- end of main content -->
</div>

@endsection