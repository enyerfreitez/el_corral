@php
    $title = 'Inclusión laboral con la Fundación Best Buddies | El Corral';
    $description = 'Gracias al convenio entre El Corral y Fundación Best Buddies hemos logrado nuevos empleos en los diferentes restaurantes del país. ¡Conócelo!';
@endphp
@extends('layouts.main')
@section('content')

<div id="wrapper">
    <main>
        <!-- main content -->
        <div class="detail full">
            <section class="full-s">
                <h1>Fundación Best Buddies</h1>
            </section>
        </div>
        <div class="content-wrap full-width">
            <section>
                <div>
                    <div class="grid gallery hidden">
                        
                        <div class="sizer"></div>
                        <div class="item">
                            <figure class="has-overlay">
                                <img src="/img/sostenibilidad/bestbuddies/banner_copy.jpg" alt="Best Buddies" class="lazyload"/>
                            </figure>
                        </div>
                        {{--
                        <div class="sizer"></div>
                        <div class="item">
                            <figure class="has-overlay">
                                <p class="justified">
                                    La fundación Best Buddies Colombia es una historia que nace de la fe, 
                                    el amor incondicional y valentía de unos padres que buscaron y crearon 
                                    un lugar mejor para su hija y tantos otros. Best Buddies Colombia le 
                                    brinda a las personas con discapacidad la oportunidad de sostener un 
                                    empleo productivo.
                                    <br>
                                    En este momento somos el cuarto mayor empleador de oportunidad laboral 
                                    y hay 48 amigos vinculados en nuestros restaurantes. En El Corral tenemos 
                                    la certeza que entre todos podemos construir una verdadera sociedad 
                                    incluyente.
                                </p>
                            </figure>
                        </div>
                        <div class="sizer"></div>
                        <div class="item">
                            <figure class="has-overlay">
                                <img src="/img/sostenibilidad/bestbuddies/banner_copy.jpg" />
                            </figure>
                        </div>
                        --}}
                        <div class="sizer"></div>
                        <div class="item">
                            <figure class="has-overlay">
                                <img src="/img/sostenibilidad/bestbuddies/Infografia BB.png" alt="Infografía Best Buddies" class="lazyload"/>
                            </figure>
                        </div>
                        <div class="sizer"></div>
                        <div class="item">
                            <figure class="has-overlay">
                                <div class="two-cols full-w">
                                    <iframe src="https://www.youtube.com/embed/KBkIKe06iYI?rel=0&autoplay=0&showinfo=0&controls=0" 
                                    frameborder="0" allowfullscreen class="video"></iframe>                
                                    <iframe src="https://www.youtube.com/embed/77B6K-K9UQI?rel=0&autoplay=0&showinfo=0&controls=0" 
                                    frameborder="0" allowfullscreen class="video"></iframe>          
                                </div>
                            </figure>
                        </div>
                    </div>
                    <div class="grid gallery hidden">
                        <div class="masonry-wrapper full-w">
                            <div class="masonry">
                                <div class="masonry-item">
                                    <div class="masonry-content has-overlay">
                                        <img src="/img/sostenibilidad/bestbuddies/testimonio_1.png" alt="Testimonio1"  class="lazyload">
                                        <h2>Israel</h2>
                                    </div>
                                </div>
                                <div class="masonry-item">
                                    <div class="masonry-content has-overlay">
                                        <img src="/img/sostenibilidad/bestbuddies/testimonio_2.png" alt="Testimonio2"  class="lazyload">
                                        <h2>Dayana</h2>
                                    </div>
                                </div>
                                <div class="masonry-item">
                                    <div class="masonry-content has-overlay">
                                        <img src="/img/sostenibilidad/bestbuddies/testimonio_3.png" alt="Testimonio3"  class="lazyload">
                                        <h2>Jhonathan</h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="grid gallery hidden">

                    </div>
                </div>     
            </section>
        </div>
        @include('layouts.footer')
        </div>
    </main>
    <!-- end of main content -->
</div>

@endsection