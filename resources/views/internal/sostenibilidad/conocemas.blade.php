@php
    $title = 'Conoce nuestra política de sostenibilidad| El Corral';
    $description = 'En Hamburguesas El Corral nos comprometemos con el desarrollo sostenible, por ello tenemos una política de gestión integral de sostenibilidad. ¡Conócela!';
@endphp
@extends('layouts.main')
@section('content')

<div id="wrapper">
    <main>
        <!-- main content -->
        <div class="detail full">
            <section class="full-s">
                <h1>Conoce mas</h1>
            </section>
        </div>
        <div class="content-wrap full-width" style="padding-bottom:0">
            <section>
                <div>
                    <div class="grid gallery hidden">
                        
                        <div class="sizer"></div>
                        <div class="item">
                            <figure class="has-overlay">
                                <img src="/img/sostenibilidad/conocemas/banner.jpg" alt="Conoce más" class="lazyload"/>
                            </figure>
                        </div>
                        <div class="item">
                            <figure class="has-overlay">
                                <img src="/img/sostenibilidad/conocemas/infografia.png" alt="Infografía Conoce Más" class="lazyload"/>
                            </figure>
                        </div>
                        {{--
                            
                        <div class="sizer"></div>
                        <div class="item">
                            <figure class="has-overlay">
                                <div class="two-cols">
                                        <p class="justified">     
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            POLÍTICA DE GESTIÓN INTEGRAL DE SOSTENIBILIDAD <br>
                                            En nuestra Organización nos comprometemos con la Gestión Integral 
                                            de nuestros riesgos para el cumplimiento de la legislación 
                                            aplicable, y para alcanzar resultados sostenibles que logren la 
                                            reducción, mitigación o eliminación de los impactos generados por 
                                            nuestras actividades. Nos comprometemos a establecer a lo largo de 
                                            nuestra cadena de valor los procesos para identificar, priorizar y 
                                            gestionar integralmente los riesgos asociados a nuestra actividad 
                                            y que impactan a todos los grupos relacionados, con especial foco 
                                            en el grupo humano, nuestros consumidores, proveedores y el medio 
                                            ambiente.    
                                        </p>         
                                    <img src="/img/sostenibilidad/conocemas/grafico.png" />          
                                </div>
                            </figure>
                        </div>
                        <div class="sizer"></div>
                        <div class="item">
                            <figure class="has-overlay">
                                <img src="/img/sostenibilidad/conocemas/definiciones.png" />
                            </figure>
                        </div>
                        --}}
                    </div>

                    {{--
                    <p class="centered">

                            SITIOS TURÍSTICOS <br>
                            En El Corral siempre te sorprendes con nuestra variada carta de platos inspirados en los
                            sabores de todo el mundo. El resultado de esta variedad está inspirada en cada uno de los
                            países que hacen de esta riqueza gastronómica transmitida a través de manos
                            especialistas orgullosamente colombianas.
                            <br>
                            Más información: <a href="http://www.colombia.co/" target="__blank">http://www.colombia.co/</a>   
                    </p>
                    --}}
                    <div class="grid gallery hidden">
                        <div class="masonry-wrapper">
                                <div class="masonry">
                                    <div class="masonry-item">
                                        <div class="masonry-content has-overlay">
                                            <img src="/img/sostenibilidad/conocemas/ciudades/bogota-01.png" alt="Dummy Image"  class="lazyload">
                                        </div>
                                    </div>
                                    <div class="masonry-item">
                                        <div class="masonry-content has-overlay">
                                            <img src="/img/sostenibilidad/conocemas/ciudades/bogota-02.png" alt="Dummy Image"  class="lazyload">
                                        </div>
                                    </div>
                                    <div class="masonry-item">
                                        <div class="masonry-content has-overlay">
                                            <img src="/img/sostenibilidad/conocemas/ciudades/bogota-03.png" alt="Dummy Image"  class="lazyload">
                                        </div>
                                    </div>
                                    <div class="masonry-item">
                                        <div class="masonry-content has-overlay">
                                            <img src="/img/sostenibilidad/conocemas/ciudades/cali-01.png" alt="Dummy Image"  class="lazyload">
                                        </div>
                                    </div>
                                    <div class="masonry-item">
                                        <div class="masonry-content has-overlay">
                                            <img src="/img/sostenibilidad/conocemas/ciudades/cali-02.png" alt="Dummy Image"  class="lazyload">
                                        </div>
                                    </div>
                                    <div class="masonry-item">
                                        <div class="masonry-content has-overlay">
                                            <img src="/img/sostenibilidad/conocemas/ciudades/cali-03.png" alt="Dummy Image"  class="lazyload">
                                        </div>
                                    </div>
                                    <div class="masonry-item">
                                        <div class="masonry-content has-overlay">
                                            <img src="/img/sostenibilidad/conocemas/ciudades/cali-04.png" alt="Dummy Image"  class="lazyload">
                                        </div>
                                    </div>
                                    <div class="masonry-item">
                                        <div class="masonry-content has-overlay">
                                            <img src="/img/sostenibilidad/conocemas/ciudades/costacaribe-01.png" alt="Dummy Image"  class="lazyload">
                                        </div>
                                    </div>
                                    <div class="masonry-item">
                                        <div class="masonry-content has-overlay">
                                            <img src="/img/sostenibilidad/conocemas/ciudades/costacaribe-02.png" alt="Dummy Image"  class="lazyload">
                                        </div>
                                    </div>
                                    <div class="masonry-item">
                                        <div class="masonry-content has-overlay">
                                            <img src="/img/sostenibilidad/conocemas/ciudades/costacaribe-03.png" alt="Dummy Image"  class="lazyload">
                                        </div>
                                    </div>
                                    <div class="masonry-item">
                                        <div class="masonry-content has-overlay">
                                            <img src="/img/sostenibilidad/conocemas/ciudades/costacaribe-04.png" alt="Dummy Image"  class="lazyload">
                                        </div>
                                    </div>
                                    <div class="masonry-item">
                                        <div class="masonry-content has-overlay">
                                            <img src="/img/sostenibilidad/conocemas/ciudades/costacaribe-05.png" alt="Dummy Image"  class="lazyload">
                                        </div>
                                    </div>
                                    <div class="masonry-item">
                                        <div class="masonry-content has-overlay">
                                            <img src="/img/sostenibilidad/conocemas/ciudades/medellin-01.png" alt="Dummy Image"  class="lazyload">
                                        </div>
                                    </div>
                                    <div class="masonry-item">
                                        <div class="masonry-content has-overlay">
                                            <img src="/img/sostenibilidad/conocemas/ciudades/medellin-02.png" alt="Dummy Image"  class="lazyload">
                                        </div>
                                    </div>
                                    <div class="masonry-item">
                                        <div class="masonry-content has-overlay">
                                            <img src="/img/sostenibilidad/conocemas/ciudades/medellin-03.png" alt="Dummy Image"  class="lazyload">
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>

                    {{--
                    <p class="centered">

                            PROGRAMA DE SOSTENIBILIDAD <br>
                            Desde El Corral, siempre ofrecemos nuestro mejor servicio y sabor de manera sostenible.
                            Es por eso que queremos compartir con todos nuestros clientes nuestro programa de
                            sostenibilidad como muestra de nuestro compromiso por ser una marca sostenible y en
                            pro de beneficiar las generaciones futuras. Conócela dando click en la imagen.
                            <br>
                            <a  class="btn" href="/pdf/politica-sostenibilidad.pdf" target="__blank">Descargar PDF</a>
                    </p>
                    --}}
                    <div class="grid gallery hidden">
                        <div class="item">
                            <figure class="has-overlay">
                                <img src="/img/sostenibilidad/conocemas/programa.png" alt="Programa Sostenibilidad"/>
                            </figure>
                        </div>
                        <div class="item">
                            <figure class="has-overlay">
                                <a href="/pdf/politica-sostenibilidad.pdf" target="__blank">
                                    <img src="/img/sostenibilidad/conocemas/descarga.png" alt="Descargar Política de Sostenibilidad"/>
                                </a>
                            </figure>
                        </div>
                    </div>
                    
                </div>     
            </section>
        </div>
        @include('layouts.footer')
        </div>
    </main>
    <!-- end of main content -->
</div>

@endsection