@php
    $title = 'Desarrollo sostenible | El Corral: La Receta Original';
    $description = '¡Porque el desarrollo sostenible también es importante! Conoce los empaques de las hamburguesas El Corral 100% fabricados con fibra de caña de azúcar.';
@endphp
@extends('layouts.main')
@section('content')

<div id="wrapper">
    <main>
        <!-- main content -->
        <div class="detail full">
            <section class="full-s">
                <h1>Sostenibilidad</h1>
            </section>
        </div>
        <div class="content-wrap full-width">
            <section>
                <div>
                    <div class="grid gallery hidden">
                        <div class="sizer"></div>
                        <div class="item">
                            <a href="/sostenibilidad/lo-de-afuera-tambien-importa">
                                <figure class="has-overlay">
                                    <img src="/img/sostenibilidad/empaques.jpg" alt="Empaques" class="lazyload"/>
                                </figure>
                            </a>
                        </div>
                        
                        <div class="sizer"></div>
                        <div class="item">
                            <a href="/sostenibilidad/fundacion-best-buddies">
                                <figure class="has-overlay">
                                    <img src="/img/sostenibilidad/bestbuddies.jpg" alt="Fundación Best Buddies" class="lazyload"/>
                                </figure>
                            </a>
                        </div>
                        
                        <div class="sizer"></div>
                        <div class="item">
                            <a href="/sostenibilidad/conoce-mas">
                                <figure class="has-overlay">
                                    <img src="/img/sostenibilidad/conoce-mas.jpg" alt="Conoce más" class="lazyload"/>
                                </figure>
                            </a>
                        </div>
                    </div>
                </div>     
            </section>
        </div>
        @include('layouts.footer')
        </div>
    </main>
    <!-- end of main content -->
</div>

@endsection