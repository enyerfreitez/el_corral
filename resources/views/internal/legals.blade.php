@php
    $breadcrumb = "TÉRMINOS Y CONDICIONES";
    $title = 'Términos y Condiciones | El Corral';
    /* $description = '¿Sabías que el empaque en el que comes tus papitas o tomas tu bebida de El Corral es biodegradable? ¡Conoce nuestro desarrollo sostenible!'; */

@endphp
@extends('layouts.main')
@section('content')

<div id="wrapper">
    <main>
        <div class="detail full">
            <section>
            <h1>Términos y Condiciones</h1>
            </section>
        </div>
        <div class="content-wrap full-width">
            <section>
                <div>
                    <div class="grid gallery hidden">
                        <div class="tab-container">
                            <div class="tab">
                                @foreach ($legals as $legal)
                                <button class="tablinks" onclick="openTab(event, '{{"tabable_" . $legal->id}}')">{{$legal->title}}</button>
                                @endforeach
                              </div>
                                @foreach ($legals as $legal)
                                <div id="{{"tabable_" . $legal->id}}" class="tabcontent">
                                  <p>{!!$legal->text!!}</p>
                                </div>
                                @endforeach
                              
                        </div>
                        
                    </div>
                </div>     
            </section>
        </div>
        @include('layouts.footer')
        </div>
    </main>    
    <!-- end of main content -->
</div>
@endsection