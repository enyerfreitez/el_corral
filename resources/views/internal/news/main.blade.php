@php
    $title = "Lo Nuevo: Domicilios en línea, Combo de combos | El Corral";
    $description = "No dudes en hacer tu domicilio de El Corral, conoce mucho más sobre nuestros nuevos  combos de combos, para disfrutar en familia.";
@endphp
@extends('layouts.main')
@section('content')

<div id="wrapper">

    <main>
        <div class="detail full">
            <section>
            <h1>Lo Nuevo</h1>
            </section>
        </div>
        <div class="content-wrap full-width">
            <section>
                <div>
                    <div class="grid gallery hidden">
                        @foreach ($news as $new)
                        <div class="sizer"></div>
                        <div class="item">
                            <figure class="has-overlay">
                                <img src="{{$new->banner_img}}" alt="{{$new->description}}" />
                                <div class="overlay">
                                    <section class="news-section">
                                        <h2 class="product-title news-h2">{{$new->title}}</h2>
                                        <p class="news2-p">
                                            {{$new->description}}  
                                            <br></p>
                                            @if (!is_null($new->custom_url))
                                            <a href="{{$new->custom_url}}" class="button" target="__blank">
                                                {{$new->button_text ? $new->button_text : "Clic aquí"}} 
                                            </a>
                                            @elseif($new->content_entry != null)
                                            {!! $new->content_entry != null ? "<a href='/lo-nuevo/$new->slug'  class='button' >" : ''!!}
                                            {{$new->button_text ? $new->button_text : "Clic aquí"}} 
                                            {!! $new->content_entry != null ? "</a>" : ''!!}
                                            @else
                                            @endif
                                    </section>
                                </div>
                            </figure>

                        </div>
                        @endforeach
                    </div>
                </div>     
            </section>
        </div>
        @include('layouts.footer')
        </div>
    </main>
    <!-- end of main content -->
</div>

@endsection