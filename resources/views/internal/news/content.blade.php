@php
    $description = $new->description;
    $title = $new->meta_title ? $new->meta_title : 'El Corral | Hamburguesas, Malteadas y Papitas';
@endphp
@extends('layouts.main')
@section('content')
<div id="wrapper">
    <main>
        <div class="detail full">
            <section>
            <h1>{{ $new->title }}</h1>
            </section>
        </div>
        <div class="content-wrap full-width">
            <section>
                <div>
                    <div class="grid gallery hidden">
                        
                        <div class="sizer"></div>
                        <div class="item">
                            <figure class="has-overlay">
                                <div class="two-cols-content-news">
                                    <div class="searchBlock">
                                        <a href="https://pideenlinea.elcorral.com/?source=pagElCorral#no-back-button">
                                            <img src="{{$new->banner_img}}" alt="{{$new->description}}" />
                                        </a>
                                        {!! $new->content_entry !!}
                                    </div>
                                    <div>
                                        @foreach ($news as $item)
                                        <a href='/lo-nuevo/{{$item->slug}}'>
                                        <img src="{{$item->campaign_tag_img}}" alt="{{$item->description}}" />
                                        </a>
                                        @endforeach
                                    </div>

                                </div>
                            </figure>
                        </div>
                        
                    </div>
                </div>     
            </section>
        </div>
        @include('layouts.footer')
        </div>
    </main>
    <!-- end of main content -->
</div>

@endsection