@php
    $description = $new->description;
    $title = $new->meta_title ? $new->meta_title : 'El Corral | Hamburguesas, Malteadas y Papitas';
@endphp
@extends('layouts.main')
@section('content')

<div id="wrapper">
    <main>
        <div class="detail full">
            <section>
            <h1>{{ $new->title }}</h1>
            </section>
        </div>
        <div class="content-wrap full-width">
            <section>
                <div>
                    <div class="grid gallery hidden">
                        @foreach ($contents as $content)
                        <div class="sizer"></div>
                        <div class="item">
                            <figure class="has-overlay">
                                @if ($content->type)
                                <img src="{{$content->content}}" />
                                @else
                                <iframe style="width:100%!important;height:500px" src="https://www.youtube.com/embed/{{$content->content}}?rel=0&autoplay=0&showinfo=0&controls=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                @endif
                            </figure>
                        </div>
                        @endforeach
                    </div>
                </div>     
            </section>
        </div>
        @include('layouts.footer')
        </div>
    </main>
    <!-- end of main content -->
</div>

@endsection