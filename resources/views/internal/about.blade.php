@php
    $description = "Conoce cómo nace nuestra cadena y sus mejores hamburguesas, perros calientes, malteadas y mucho más. ¡Visita uno de nuestros 210 restaurantes del país!";
    $title = 'Conoce más sobre nosotros | El Corral: La Receta Original';
    $schema = '
    <script type="application/ld+json"> 
    {
    "@context": "http://www.schema.org",
    "@type": "AboutPage",
    "name": "Nosotros",
    "description" : "'.$description.'",
    "alternateName": "Acerca del Corral",
    "url": "https://elcorral.com/nosotros"
    }
    </script>
    '

@endphp
@extends('layouts.main')
@section('content')

<div id="wrapper">
    <main>
        <div class="content-wrap full-width">
            <section>
                <div>
                    <div class="grid gallery has-display">
                        <div class="item">
                            <figure class="has-overlay" style="margin-top: 13vh">
                                <img src="/img/about/v2/Banner Nosotros.jpg" class=" lazyload" alt="Historia del Corral"/>
                            </figure>
                        </div>
                        <div class="item centered">
                            <figure class="nosotros">        
                                <p class="infography centered" style="font-size: 30px; text-align:justify!important; padding:10px">
                                     El Corral inició operaciones en 1983, con la inauguración se su primer restaurante de comida casual de autoservicio en Bogotá. 
                                    El estilo original de su decoración, que combina elementos pop y retro, nació en ese local.</p>
                                   
                                    <img src="/img/about/v2/Logos PNG.png" class="infography lazyload" alt="Historia del Corral"/><br>
                                    <p class="infography centered" style="font-size: 30px; text-align:justify!important; padding:10px">
                                    Desde la inauguración del primer restaurante, nuestro compromiso ha sido satisfacer tus gustos, 
                                    expectativas y necesidades con una excelente calidad. Por esta razón, 
                                    utilizamos materias primas seleccionadas para la preparación de los alimentos; además, en forma permanente, 
                                    investigamos y renovamos los procedimientos y técnicas empleada.</p>    
                                    
                                    <p class="infography centered"><img src="/img/about/v2/Mapa PNG.png" class="infography lazyload" alt="Historia del Corral"/></p>
                                    <p class="infography centered" style="font-size: 30px; text-align:justify!important; padding:10px">
                                    En el 2013 cumplimos 30 años inaugurando nuestro primer restaurante 24 horas para que nuestros clientes disfruten La Receta Original
                                    en todo momento.</p>
                                    
                                    <p class="infography centered"><img src="/img/about/v2/Lema PNG.png" class="infography lazyload" alt="Historia del Corral"/></p>
                                    <p class="infography centered" style="font-size: 30px; text-align:justify!important; padding:10px">
                                    En estos 38 años hemos querido llevar La Receta Original – The Original Recipe a más lugares en el mundo 
                                    y nos encontramos como modelo de franquicia en Ecuador.</p>
                                    
                                    <p class="infography centered"><img src="/img/about/v2/Nuestra Gente PNG.png" class="infography lazyload" alt="Historia del Corral"/></p>
                                    <p class="infography centered" style="font-size: 30px; text-align:justify!important; padding:10px">
                                    Nuestros clientes son la razón de ser, pero la gente es la fuente de nuestra fortaleza. El capital humano, su compromiso, 
                                    su profesionalismo y dedicación son factores determinantes del éxito, crecimiento y aceptación de nuestras cadenas. </p>
                                    
                                    <p class="infography centered"><img src="/img/about/v2/Hamburguesa PNG.png" class="infography lazyload" alt="Historia del Corral"/><p>
                                    <p class="infography centered" style="font-size: 30px; text-align:justify!important; padding:10px">
                                        EN HAMBURGUESAS EL CORRAL todos los productos son elaborados cuando se ordenan, para garantizar su frescura y calidad.
                                        Vegetales seleccionados, pan fresco, productos lácteos de primera calidad y aceite vegetal sin colesterol para freír, 
                                        complementan el sabor característico e inconfundible de nuestros productos. </p>
                                    
                                        <p class="infography centered"><img src="/img/about/v2/La Variedad PNG.png" class="infography lazyload" alt="Historia del Corral"/><p>
                                    <p class="infography centered" style="font-size: 30px; text-align:justify!important; padding:10px">
                                        La variedad del menú de El Corral es tan extensa como su clientela. Buscando satisfacer sus diversos gustos, 
                                        el menú a crecido con el paso de los años y continua en constante evolución.
                                        La venta está garantizada por nuestros sistemas de Gestión de Calidad, Seguridad Alimentaria y por el aseguramiento de los procesos para la producción.
                                         Todo con el fin de lograr la plena satisfacción de los clientes.
                                    </p>
                            </figure>
                        </div>
                        {{--
                        <div class="item centered">
                            <figure class="has-overlay">
                                <img src="/img/about/Evolucion Logo.png"/>
                            </figure>
                        </div>
                        
                        <div class="sizer"></div>
                        <div class="item centered need-top-padding" style="margin-left:15%; margin-right:15%">
                                <h2 class="orange-title">Historia</h2>
                                <img src="/img/about/Fechas.png"/>
                                <p class="justified text">     
                                El Corral inició operaciones en 1983‚ con la inauguración de su primer restaurante de comida casual 
                                de autoservicio en Bogotá. El estilo original de su decoración, que combina elementos pop y retro‚ 
                                nació en ese local.
                                <br>
                                <br>
                                El lema La Receta Original  – The Original Recipe  se convierte en el hito y futuro de la marca.
                                <br>
                                <br>
                                Desde la inauguración del primer restaurante, nuestro compromiso ha sido satisfacer tus gustos‚ 
                                expectativas y necesidades con una excelente calidad. Por esta razón, utilizamos materias primas 
                                seleccionadas para la preparación de los alimentos; además, en forma permanente, investigamos y 
                                renovamos los procedimientos y técnicas empleadas.
                                <br>
                                <br>
                                La cadena se encuentra en continuo desarrollo desde su fundación. Hoy cuenta con 217 restaurantes 
                                en 42 ciudades y municipios de todo el país.  En el 2013 cumplimos 30 años inaugurando nuestro primer 
                                restaurante 24 horas para que nuestros clientes disfruten La Receta Original en todo momento.
                                <br>
                                <br>
                                <strong> Nuestra Gente </strong> <br>
                                En estos 35 años hemos querido llevar La Receta Original - The Original Recipe a más lugares en el 
                                mundo y nos encontramos como modelo de franquicia en  Ecuador.
                                <br>
                                <br>
                                Nuestros clientes son la razón de ser‚ pero la gente es la fuente de nuestra fortaleza. El capital 
                                humano, su compromiso‚ su profesionalismo y dedicación son factores determinantes del éxito, 
                                crecimiento y aceptación de nuestras cadenas.
                                <br>
                                
                            </p>
                            <figure>

                                <div class="three-cols-content">
                                    <div>
                                        <img src="/img/about/Hamburgesa-y-papas.png" alt="">
                                    </div>
                                    <div >
                                        <h1>
                                            La Receta Original
                                            <br>
                                            The Original Recipe
                                        </h1>
                                    </div>
                                    <div>
                                        <img src="/img/about/Sal.png" alt="">
                                    </div>
                                </div>
                            </figure>
                            <p class="justified text">

                                En Hamburguesas El Corral, todos los productos son elaborados cuando se ordenan‚ para garantizar 
                                su frescura y calidad. 
                                <br>
                                <br>
                                Vegetales seleccionados‚ pan fresco‚ productos lácteos de primera calidad y aceite vegetal sin 
                                colesterol para freír, complementan el sabor característico e inconfundible de nuestros productos.
                                <br>
                                <br>
                                La variedad del menú de El Corral, es tan extensa como su clientela. Buscando satisfacer sus 
                                diversos gustos‚ el menú ha crecido con el paso de los años y continúa en constante evolución.
                                <br>
                                <br>
                                La venta está garantizada por nuestros sistemas de Gestión de Calidad‚ Seguridad Alimentaria y 
                                por el aseguramiento de los procesos para la producción. Todo con el fin de lograr la plena 
                                satisfacción de los clientes.
                                
                            </p> 
                            <h2>CONTÁCTANOS AL 018000114722 O ESCRÍBENOS A WWW.ELCORRAL.COM</h2>

                        </div>--}}
                    </div>
                </div>     
            </section>
        </div>
        @include('layouts.footer')
        </div>
    </main>
    <!-- end of main content -->
</div>

@endsection