@php
    $x = $products->count();
    $y = null;
    if($x % 3 == 0){}
    elseif(($x + 1)% 3 == 0){
        $y = 1;
    }
    elseif(($x + 2)% 3 == 0){
        $y = 2;
    }
    /* Metadata */
    $description = $category->description;
    $title = $category->meta_title ? $category->meta_title : 'Nuestra Carta '. $category->name.' | El Corral: La Receta Original';
@endphp
@extends('layouts.main')
@section('content')

<div id="wrapper">
    <main>
    <!-- main content -->
    <div class="detail full">
        <section>
            <h1>{{ $category->name }}</h1>
        </section>
    </div>
    <div class="content-wrap full-width">
        <section>
            <div>
                <div class="grid gallery hidden">
                    <div class="masonry-wrapper">
                        <div class="masonry">
                            @if($y == 2)
                            <div class="masonry-item">
                                <div class="masonry-content has-overlay">
                                    <img src="/img/quotes/BG {{ random_int(1,11) }}.png" alt="quote" class="lazyload">
                                    <h2 class="product-title"> &nbsp; </h2>
                                </div>
                            </div>
                            @endif
                            @foreach ($products as $product)
                            <div class="masonry-item">
                                <div class="masonry-content has-overlay">
                                    <img src="{{ $product->img_uri }}" alt="El  Corral - {{ $category->name }}: {{ $product->name }}" class="lazyload">
                                    <div class="overlay">
                                        <section>
                                            <p>{{ $product->subcategory }}</p>
                                            <p>{!! nl2br(e($product->description))!!}</p>
                                            @if ($product->online_sale)
                                            <a href="https://pideenlinea.elcorral.com/FrmLocation.aspx?source=pagElCorral#no-back-button" target="__blank">
                                                <img src="/img/logo/shop_cart_w.png" alt="Comprar {{ $product->name }}" style="width: 60px!important">
                                            </a>
                                            @endif
                                        </section>
                                    </div>    
                                    <h2 class="product-title">{{ $product->name }}</h2>              
                                </div>
                            </div>
                            @endforeach
                            @if($y >= 1)
                            <div class="masonry-item">
                                <div class="masonry-content has-overlay">
                                    <img src="/img/quotes/BG {{ random_int(1,11) }}.png" alt="quote" class="lazyload">
                                    <h2 class="product-title"> &nbsp; </h2>
                                </div>
                            </div>
                            @endif
                        @if ($category->slug == 'hamburguesas')
                        <h2 class="help-text">¡Pídela en combo con papas y tu bebida favorita!</h2>
                        @elseif ($category->slug == 'wraps')
                        <h2 class="help-text">todas nuestras hamburguesas y sandwiches 
                                se pueden pedir en wrap integral o wrap lechuga</h2>
                        @elseif($category->slug == 'ensaladas')
                        <h2 class="help-text">las puedes acompañar con uno de nuestros deliciosos aderezos</h2>
                        @elseif($category->slug == 'corralito-infantil')
                        <h2 class="help-text">Disfruta de estos corralitos y más en nuestras fiestas infantiles. <a href="/eventos-fiestas-y-bonos/fiestas">¡Entérate Aquí!</a></h2>
                        @elseif($category->slug == 'para-compartir')
                        <h2 class="help-text">aplica sólo para algunos restaurantes</h2>
                        @elseif($category->slug == 'pa-tardear')
                        <h2 class="help-text">Pídelos de lunes a viernes de 2 a 6 PM. No aplica a San Andrés.</h2>
                        @elseif($category->slug == 'desayunos')
                        <h2 class="help-text">todos se pueden pedir en wrap, pan o arepa.
                        <a class="button" href="/pdf/Desayunos.pdf" target="__blank">Descarga aquí</a> </h2>
                        <div class="grid gallery hidden">
                            <div class="sizer"></div>
                            <div class="item">
                                <figure class="has-overlay">
                                    <img src="/img/infografia-restaurantes-desayunos-el-corral.png" class="lazyload" alt="Infografía restaurantes desayunos El Corral" title="Infografía restaurantes desayunos El Corral" />
                                </figure>
                            </div>
                        </div>
                        @endif
                        </div>
                    </div>
                </div>
                
                <ul class="cats">
                    @foreach ($categories as $cate)

                    <li>
                        <a class="{{ $category->slug == $cate->slug ? 'active' : '' }}" href="{{ '/nuestra-carta/' . $cate->slug }}">{{ $cate->name }}</a>
                    </li>
                    @endforeach
                </ul>
            </section>
        </div>
        @include('layouts.footer')
    </div>
    </main>
    <!-- end of main content -->
</div>

@endsection