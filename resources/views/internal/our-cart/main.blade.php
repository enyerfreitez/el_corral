@php
    /* Metadata */
    $description = 'Descubre nuestra carta y haz tu pedido prueba nuestras hamburguesas, vaqueros (perros calientes), wraps, malteadas, ensaladas, helados, papitas y mucho más.';
    $title = "Nuestra Carta | El Corral: La Receta Original";
    $schema = '
    <script type="application/ld+json">
    {
    "@context" : "http://schema.org",
    "@type" : "Product",
    "name" : "Nuestra Carta",
    "description" : "'.$description.'",
    "url" : "https://www.elcorral.com/nuestra-carta",
    "brand" : {
        "@type" : "Brand",
        "name" : "El Corral © 2020",
        "logo" : "https://www.elcorral.com/img/logo/logo.png"
    }
    }
    </script>
    ';
@endphp

@extends('layouts.main')
@section('content')

<div id="wrapper">
    <main>
    <!-- main content -->
        <div class="detail full">
            <section class="full-s">
                <h1>Nuestra Carta</h1>
                <div class="menu" id="menuItems">
                    <span class="button" id="d-menu">
                        Descargar Carta <img src="/img/icono_descargas.png" alt="Ícono descargas" title="Ícono descargas">
                    </span>
                    <ul id="d-list">
                        <li><a class="d-menu-item" href="/pdf/EC_Menu_Digital.pdf" target="__blank"> Menú General </a> </li>
                        <li><a class="d-menu-item" href="/pdf/EC_Menu_Digital_San_Andres.pdf" target="__blank">Menú San Andrés </a> </li>
                        <li><a class="d-menu-item" href="/pdf/EC_Menu_Digital_Aeropuerto.pdf" target="__blank">Aeropuerto Bogotá</a> </li>
                        <li><a class="d-menu-item" href="/pdf/EC_Menu_Digital_Universidades.pdf" target="__blank">Javeriana/Andes</a> </li>
                    </ul>
                </div>

            </section>
        </div>
        <div class="content-wrap full-width">
            <section>
                <div>
                    <div class="grid gallery hidden">
                        @foreach ($categories as $category)
                        <div class="sizer"></div>
                        <div class="item">
                            <a href="nuestra-carta/{{ $category->slug }}">
                                <figure class="has-overlay">
                                <img src="{{ $category->img_uri }}"  alt="{{ $category->name }}" class="ladyload"/>
                                    <div class="overlay">
                                        <section>
                                            <h2>{{ $category->name }}</h2>
                                        </section>
                                    </div>
                                </figure>
                            </a>
                        </div>
                        @endforeach
                        <div class="sizer"></div>
                        <div class="item">
                            <figure class="has-overlay">
                                <h2 class="help-text">  imágenes de referencia. </h2>
                            </figure>
                        </div>    
                    </div>
                </div>     
                
            </section>
            
        </div>
        @include('layouts.footer')
    </div>
</main>
    <!-- end of main content -->
</div>

@endsection