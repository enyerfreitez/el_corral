@php
    $title = 'Foodtrucks y eventos | El Corral: la receta original';
    $description = 'Conoce el Foodtruck de El Corral donde te llevaremos las mejores hamburguesas, perros calientes, wraps, papas fritas, malteadas, helados y más a tu evento.';
@endphp
@extends('layouts.main')
@section('content')

<div id="wrapper">
    <main>
        <div class="detail full">
            <section>
                <h1>Foodtruck y Eventos</h1>
            </section>
    </div>
        <div class="content-wrap full-width hidden">
            <section>
                <div>
                    <div class="grid gallery has-display">
                        
                    <div class="sizer"></div>
                    <div class="item">
                        <figure class="has-overlay">
                            <img src="/img/side-services/ft/Banner Interna FT.jpg" alt="Nuestro Foodtruck" class="lazyload"/>
                        </figure>
                    </div>
                    <div class="sizer"></div>
                    <div class="item">
                        <figure class="has-overlay">
                            <img src="/img/side-services/ft/Infografia FT.png" alt="Infografía Foodtruck" class="lazyload"/>
                        </figure>
                    </div>
                    <div class="sizer need-top-padding"></div>
                    <div class="item centered has-display">
                        <h2>¿Quieres saber más?</h2>
                        <a href="/hablanos" class="button">
                            ¡Contáctanos!
                        </a>
                        <br>
                        <a href="tel:0316543354">
                            <img src="/img/contact/Llamanos-Bogota.png" alt="" style="width:auto;" class="need-top-padding lazyload">
                        </a>
                        <br>
                        <a href="tel:3336222122">
                            <img src="/img/contact/Llamanos-Resto-del-pais.png" style="width:auto;" class="need-top-padding lazyload">
                        </a>
                    </div>
                    <div class="sizer need-top-padding"></div>
                    <div class="item centered has-display">
                        <figure>
                            <iframe style="width:100%!important;height:500px" src="https://www.youtube.com/embed/QnMU4NrF9YM?rel=0&autoplay=0&showinfo=0&controls=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        </figure>
                    </div>
                    
                </div>
            </section>
        </div>
        @include('layouts.footer')
        </div>
    </main>
    <!-- end of main content -->
</div>

@endsection


{{--
    <h1>¿Qué Incluye</h1>
    <p class="justified">     
        AMBIENTACIÓN TIPO PICNIC
        Puff, parasoles y colchón estilo Corral para vivir una experiencia completa 
        a la hora de comerte tu hamburguesa favorita.
    </p>         
    <h1>TEN EN CUENTA LA SIGUIENTE INFORMACIÓN</h1>
    <p class="justified">     
        AMBIENTACIÓN TIPO PICNIC
        Puff, parasoles y colchón estilo Corral para vivir una experiencia completa 
        a la hora de comerte tu hamburguesa favorita.
    </p>         
    <p class="justified">     
        AMBIENTACIÓN TIPO PICNIC
        Puff, parasoles y colchón estilo Corral para vivir una experiencia completa 
        a la hora de comerte tu hamburguesa favorita.
    </p>         
    <p class="justified">     
        AMBIENTACIÓN TIPO PICNIC
        Puff, parasoles y colchón estilo Corral para vivir una experiencia completa 
        a la hora de comerte tu hamburguesa favorita.
    </p>  
    
    --}}