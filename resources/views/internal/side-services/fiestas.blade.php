@php
    $description =  'Celebra en El Corral el día especial de los que más quieres con nuestras fiestas infantiles.';
    $title =  'Fiestas El Corral | La Receta Original';
@endphp
@extends('layouts.main')
@section('content')

<div id="wrapper">
    <main>
        <div class="detail full">
            <section>
                <h1>Fiestas Infantiles</h1>
            </section>
    </div>
    <div class="content-wrap full-width">
        <div class="content-wrap full-width">
            <section>
                <div>
                    <div class="grid gallery hidden has-text-centered">
                        
                        <div class="sizer"></div>
                        <div class="item">
                            <figure class="has-overlay">
                                <img src="/img/side-services/fiestas/F2.jpg" alt="Fiestas Infantiles" class="lazyload"/>
                            </figure>
                        </div>
                        <div class="sizer"></div>
                        <div class="item">
                            <figure class="has-overlay">
                                <img src="/img/side-services/fiestas/F1.jpg" alt="Infografía Fiestas Infantiles" class="lazyload"/>
                            </figure>
                        </div>
                    </div>
                </div>     
            </section>
        </div>
        @include('layouts.footer')
        </div>
    </main>
    <!-- end of main content -->
</div>

@endsection



