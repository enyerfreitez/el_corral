@php
    $title = 'Bonos El Corral | La Receta Original';
    $description = 'Utiliza nuestros bonos para que los regales como incentivos, detalles, premios o beneficios.';
@endphp
@extends('layouts.main')
@section('content')

<div id="wrapper">
    <main>
        <div class="detail full">
            <section>
                <h1>Bonos</h1>
            </section>
    </div>
        <div class="content-wrap full-width hidden">
            <section>
                <div>
                    <div class="grid gallery has-display">
                        
                        <div class="sizer"></div>
                        <div class="item">
                            <figure class="has-overlay">
                                <img src="/img/side-services/bonos/banner_new.jpg" alt="Nuestros Bonos" class="lazyload"/>
                            </figure>
                        </div>
                        <div class="sizer"></div>
                        <div class="item">
                            <figure class="has-overlay">
                                <img src="/img/side-services/bonos/Interna Bonos.png" alt="Infografía Bonos" class="lazyload"/>
                            </figure>
                        </div>
                        {{--
                        <div class="sizer"></div>
                        <div class="item centered need-top-padding">
                            <h1>¡Llévanos donde quieras!</h1>
                            <p class="centered">     
                                Haz feliz a tus colaboradores, clientes, proveedores, o a quien desees regalar alguno de nuestros Bonos de Regalo. <br>
                                Adquierelos en cualquiera de nuestros restaurantes de El Corral a nivel nacional.
                                Regala Experiencias, Regala El Corral. <br>
                                
                            </p> 
                            <h2>CONTÁCTANOS AL 018000114722 O ESCRÍBENOS A WWW.ELCORRAL.COM</h2>

                        </div>
                        <div class="sizer"></div>
                        <div class="item centered">
                            <figure class="has-overlay">
                                <img src="/img/side-services/bonos/Bonos.png" style="max-width:30%"/>
                            </figure>
                            <h1>Nuestros Bonos</h1>
                        </div>
                        <div class="sizer"></div>
                        <div class="item">
                            <figure class="has-overlay">
                                <div class="two-cols-1-by-2">
                                    <div>
                                        <h2>Bono producto</h2>
                                        <p class="justified">     
                                                <strong>¿QUÉ SON?</strong>   Bono configurado con productos específicos de nuestro menú, los cuales son redimibles únicamente por el alimento seleccionado. 
                                                ¡Válidos en cualquier punto de venta de El Corral a nivel nacional! <br>
                                                <strong>¿CÓMO ADQUIRIRLOS?</strong>  Comunicándote con nuestra línea de atención 018000114722. <br>
                                                <strong>CANAL VENTA:</strong>  Venta mínima de 100 bonos Producto, unicamente por medio del canal institucional. <br> 
                                                <strong>RECOMENDADO PARA:</strong>  Personas Jurídicas que deseen regalar a sus empleados, clientes o proveedores como incentivos, cumpleaños o beneficios.
                                                
                                            
                                        </p>         
                                    </div>
                                    <div>

                                        <h2>Bono regalo</h2>
                                        <p class="justified">     
                                                <strong>¿QUÉ SON?</strong>  Tarjeta de regalo que puede ser redimible parcial o totalmente por el valor en pesos que haya sido activada, 
                                                para que compres lo que quieras en cualquier restaurante de El Corral a nivel nacional. <br>

                                                <strong>¿CÓMO ADQUIRIRLOS?</strong>  En cualquiera de nuestros restaurantes a nivel nacional o por medio de nuestro Canal Institucional comunicándote en la línea de atención 018000114722. <br>
                                                
                                                CARGAS: Mínimo $20.000 máximo $100.000, en múltiplos de $10.000 ($20.000, $30.000, $40.000, $50.000, $60.000 $70.000, $80.000, $90.000, $100.000) <br>
                                                
                                                <strong>CLIENTE OBJETIVO:</strong>  Cualquier persona que desee regalar algo especial para celebraciones, incentivos o beneficios. <br>
                                                
                                                <strong>CANAL VENTA:</strong>  Puntos de venta a nivel nacional o por medio de nuestro Canal Institucional comunicándote en la línea de atención 018000114722. <br>
                                                
                                                <strong>¿DONDE LOS ADQUIERO?</strong> 
                                                Puntos de venta El Corral a nivel nacional y por la línea 018000114722.
                                                
                                            
                                        </p>         
                                    </div>
                                  
                                </div>
                            </figure>
                        </div>
                        --}}
                    </div>
                </div>     
            </section>
        </div>
        @include('layouts.footer')
        </div>
    </main>
    <!-- end of main content -->
</div>

@endsection