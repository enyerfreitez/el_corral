@php
    $title = 'Eventos, fiestas y bonos de regalo | El Corral';
    $description = 'Disfruta de la receta original de El Corral en tus eventos y fiestas infantiles y no te pierdas nuestros bonos regalo perfectos para cualquier ocasión.';
@endphp
@extends('layouts.main')
@section('content')

<div id="wrapper">
    <main>
        <div class="detail full">
                <section>
                    <h1>Eventos, Fiestas y Bonos</h1>
                </section>
        </div>
        <div class="content-wrap full-width">
            <section>
                <div>
                    <div class="grid gallery hidden">
                        <div class="sizer"></div>
                        <div class="item">
                            <a href="/eventos-fiestas-y-bonos/bonos">
                                <figure class="has-overlay">
                                    <img src="/img/side-services/Banner Bonos.jpg" alt="Bonos" class="lazyload">
                                </figure>
                            </a>
                        </div>
                        
                        <div class="sizer"></div>
                        <div class="item">
                            <a href="/eventos-fiestas-y-bonos/foodtruck-y-eventos">
                                <figure class="has-overlay">
                                    <img src="/img/side-services/Banner FT.jpg" alt="Foodtruck" class="lazyload"/>
                                </figure>
                            </a>
                        </div>
                        <div class="sizer"></div>
                        <div class="item">
                            <a href="/eventos-fiestas-y-bonos/fiestas">
                                <figure class="has-overlay">
                                    <img src="/img/side-services/Banner Fiestas.jpg" alt="Fiestas Infantiles" class="lazyload"/>
                                </figure>
                            </a>
                        </div>
                    </div>
                </div>     
            </section>
        </div>
        @include('layouts.footer')
        </div>
    </main>
    <!-- end of main content -->
</div>

@endsection