@extends('layouts.main')
@section('content')

<div id="wrapper">
    <main>
        <div class="content-wrap full-width" style="padding-bottom:0;">
            <section>
                <div>
                    <div class="grid gallery hidden">
                        <div class="sizer need-top-padding"></div>
                        <div class="item">
                            <figure class="has-overlay">
                                <a href="/">
                                    <img src="/img/errors/ERROR-2.jpg" alt="500 Error de servidor">
                                </a>
                            </figure>
                        </div>
                    </div>
                </div>     
            </section>
        </div>
        @include('layouts.footer')
        </div>
    </main>
    <!-- end of main content -->
</div>

@endsection