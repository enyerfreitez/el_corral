@extends('layouts.main')
@section('content')

    <div id="wrapper">
        <main>
        <!-- main content -->
        @include('components.slider')
        <div class="spacer"></div>
            <!-- The navigation -->
            <div class="strokes">
                <ul id="navigation">
                    <li>
                        <a href="https://pideenlinea.elcorral.com/?source=pagElCorral" target="__blank">
                            <section>
                                <h1>Domicilios:<br >Pide en Línea</h1>
                                <h5 class="badge-rounded"> A un solo clic </h5>
                            </section>
                        </a>
                    </li>
                    {{--                     
                        <li>
                            <a href="/horarios-para-llevar" data-transition="slide-to-top">
                                <section>
                                    <h2>Servicios<br>Restaurantes</h2>
                                    <h5 class="badge-rounded"> ¡Llámanos! o pasa y lleva </h5>
                                </section>
                            </a>
                        </li> 
                    --}}
                    <li>
                        <a href="/nuestra-carta" data-transition="slide-to-top">
                            <section>
                                <h2>Nuestra Carta</h2>
                                <h5 class="badge-rounded"> Antójate de nuestros productos </h5>
                            </section>
                        </a>
                    </li>
                    <li>
                        <a href="/lo-nuevo" data-transition="slide-to-top">
                            <section>
                                <h2>Lo Nuevo</h2>
                                <h5 class="badge-rounded"> Entérate de las novedades </h5>
                            </section>
                        </a>
                    </li>
                    <li>
                        <a href="/restaurantes" data-transition="slide-to-top">
                            <section>
                                <h2>Restaurantes</h2>
                                <h5 class="badge-rounded"> Encuéntranos </h5>
                            </section>
                        </a>
                    </li>
                    <li>
                        <a href="/hablanos" data-transition="slide-to-top">
                            <section>
                                <h2>Háblanos</h2>
                                <h5 class="badge-rounded"> ¡Comunícate aquí! </h5>
                            </section>
                        </a>
                    </li>
                    <li>
                        <a href="/sostenibilidad" data-transition="slide-to-top">
                            <section>
                                <h2>Sostenibilidad</h2>
                                <h5 class="badge-rounded"> ¡Conoce más! </h5>
                            </section>
                        </a>
                    </li>
                    <li>
                        <a href="/eventos-fiestas-y-bonos" data-transition="slide-to-top">
                            <section>
                                <h2>Eventos, <br > Fiestas y Bonos</h2>
                                <h5 class="badge-rounded"> ¿Ya los conoces? </h5>
                            </section>
                        </a>
                    </li>
                </ul>
            </div>

            <!-- End navigation -->
            <div class="overlay"></div>
            @include('layouts.footer')
        </main>
        <!-- end of main content -->
    </div>


@endsection


















