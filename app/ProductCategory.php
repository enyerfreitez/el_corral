<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{
    protected $table = 'products_categories';


    public function products(){
      return $this->hasMany('App\Product', 'pcategory_id', 'id');
    }

        /**
    * @see Overrides delete() method to errase all data files related with the P. Category
    * @param None
    * @return None
    */
    public function delete(){
      $arr = explode("/",$this->img_uri);
      $name = end($arr);
      \Storage::disk('our-cart')->delete($name);
      parent::delete();
  }
}
