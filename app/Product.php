<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public $timestamps = false;

    /**
    * @see Overrides delete() method to errase all data files related with the Product
    * @param None
    * @return None
    */
    public function delete(){
        $arr = explode("/",$this->img_uri);
        $name = end($arr);
        \Storage::disk('our-cart')->delete($name);
		parent::delete();
    }

    public function productCategory(){
        return $this->belongsTo('App\ProductCategory', 'pcategory_id');
      }
}
