<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProductCategory;
use App\Restaurant;
use App\BannerRestaurant;
use App\Slide;
use App\News;
use App\Contingencia;
use App\Legal;

class LandingController extends Controller
{
    public function getHome(){
        return view('app')->with('slides', Slide::all()->sortBy('order'));
    } 

    public function fetchProductCategories(Request $request){
        return view('internal.our-cart.main')->with(['categories' => ProductCategory::all()->sortBy('order')
        ]);
    }
    public function fetchProducts(Request $request, $category_slug){
        $cat = ProductCategory::where('slug', $category_slug)->first();
        if(!$cat) abort(404);

        return view('internal.our-cart.category')->with([
            'category' => $cat, 
            'products' => $cat->products->sortBy('order'), 
            'categories' => ProductCategory::all()->sortBy('order')
        ]);

    }

    public function getRestaurants(){
        $cities = Restaurant::select('city')->distinct()->orderBy('city')->get();
        $banner_restaurant = BannerRestaurant::first();
        return view('internal.restaurants.main')->with(['cities' => $cities,'banner_restaurant'=>$banner_restaurant]);
    }

    public function fetchRestaurants(Request $request){
        if ($request->city) {
            $restaurants = Restaurant::where("city", $request->city)->get();
        }else{
            $restaurants = Restaurant::fetchByPosition($request->lat, $request->lng, $request->excluded);
        }
        return response([
            'restaurants' => $restaurants
        ]);
    }

    public function indexNews(){
        return view('internal.news.main')->with('news', News::all()->sortBy('order'));
    }
    public function showNew($slug){
        $new = News::where('slug', $slug)->first();
        if(!$new) abort(404);

        return view('internal.news.content')->with([
            'new' => $new, 'contents' => $new->contents->sortBy('order'),
            'news' => /* News::all() */News::where('id', '<>', $new->id)->orderBy('order', 'ASC')->get()
            ]);
    }
    
    public function covid19(Request $request){
        return view('covid19')->with(['pdv' => Contingencia::where('active', true)->orderBy('pdv','ASC')->get(), 'cities' => Contingencia::select('city')->orderBy('city','ASC')->distinct()->get()]);
    }

    public function fetchPdv(Request $request){
        $query = Contingencia::where('active', true);
        $request->city != "all" ? $query->where("city", $request->city) : null;
        $request->takeout === "true" ? $query->where("pick_able", true) : null;
        $request->rappi === "true" ? $query->where("rappi", true) : null;
        $request->own_delivery === "true" ? $query->where("own_delivery", true) : null;
        $request->reopening === "true" ? $query->where("reopening", true) : null;

        $restaurants = $query->orderBy('pdv','ASC')->get();
        return response([
            'pdv' => $restaurants
        ]);
    }

    public function legals(){
        return view("internal.legals")->with('legals', Legal::all());
    }
}
