<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{Slide, Product, ProductCategory, Restaurant, News, Content, BannerRestaurant, Contingencia, Legal };
use Intervention\Image\ImageManagerStatic as Image;

class AdminController extends Controller
{
    public function __init(){

    }
    public function index(Request $request){
        return view('admin.index')
        ->with([
            'slides' => Slide::all()->sortBy('order'),
            'products' => Product::all(),
            'categories' => ProductCategory::all()->sortBy('order'),
            'restaurants' => Restaurant::all(),
            'legals' => Legal::all(),
            'banner_restaurant' => BannerRestaurant::first(),
            'news' => News::all()->sortBy('order'),
        ]);
    }
    /** Slides Admin 
     */
        public function slideForm(Request $request){
            $slide = Slide::find($request->id);

            return $slide ? view('admin.update.slide')->with('slide', $slide) : redirect('adm');
        }


        public function handleSlide(Request $request){

            $slide = $request->slide_id ? Slide::find($request->slide_id) : new Slide();
            $slide->title = $request->title ? $request->title : $slide->title;
            $slide->copy = $request->copy ? $request->copy : $slide->copy;
            $slide->button_text = $request->button_text ? $request->button_text : $slide->button_text;
            $slide->uri = $request->uri ? $request->uri : $slide->uri;
            
            if($request->file('banner')){
                if($request->slide_id){
                    $arr = explode("/",$slide->img_uri);
                    $name = end($arr);
                    \Storage::disk('sliders')->delete($name);
                }
                $mime = "." . explode("/",$request->file('banner')->getClientMimeType())[1];
                $name = date('Ymdhis') . $mime;
                $slide->img_uri = '/img/sliders/' . $name;
                \Storage::disk('sliders')->put($name, file_get_contents( $request->file('banner')->getRealPath() ) );
            }
            $slide->save();
        
            return redirect('adm')->with([
                'success' => true
            ]);
        }
        public function deleteSlide(Request $request){
            $slide = Slide::find($request->slide_id);
            $slide->delete();
            return redirect('adm')->with([
                'success' => true
            ]);
        }
        public function sortSlides(){
            //
        }
    /** Products Admin 
     */
        public function handleProduct(Request $request){

            $product = $request->product_id ? Product::find($request->product_id) : new Product();
            $product->name = $request->name ? $request->name : $product->name;
            $product->description = $request->description ? $request->description : $product->description;
            $product->pcategory_id = $request->category ? $request->category : $product->category;
            $product->subcategory = $request->subcategory ? $request->subcategory : $product->subcategory;
            
            if($request->file('img_uri')){
                if($request->product_id){
                    $arr = explode("/",$product->img_uri);
                    $name = end($arr);
                    \Storage::disk('our-cart')->delete($name);
                }
                $mime = "." . explode("/",$request->file('img_uri')->getClientMimeType())[1];
                $name = "el-corral-" . strtolower(str_replace(' ', '-', ProductCategory::find($request->category)->name)) . '-' . strtolower(str_replace(' ', '-', $product->name)) . '.' . $mime;
                $product->img_uri = '/img/our-cart/' . $name;
                \Storage::disk('our-cart')->put($name, file_get_contents( $request->file('img_uri')->getRealPath() ) );
            }
            $product->save();
        
            return redirect('adm')->with([
                'success' => true
            ]);
        }
        public function deleteProduct(Request $request){
            $product = Product::find($request->product_id);
            $product->delete();
            return redirect('adm')->with([
                'success' => true
            ]);
        }
        public function productForm(Request $request){
            $product = Product::find($request->id);
            return $product ? view('admin.update.product')->with('product', $product)->with('categories', ProductCategory::all()) : redirect('adm');
        }

    /** Categories Admin 
     */
        public function handleCategory(Request $request){

            $category = $request->category_id ? ProductCategory::find($request->category_id) : new ProductCategory();
            $category->name = $request->name ? $request->name : $category->name;
            $category->meta_title = $request->meta_title ? $request->meta_title : $category->meta_title;
            $category->description = $request->description ? $request->description : $category->description;
            $category->slug = str_replace(' ','-',strtolower($category->name));
            
            if($request->file('img_uri')){
                if($request->category_id){
                    $arr = explode("/",$category->img_uri);
                    $name = end($arr);
                    \Storage::disk('our-cart')->delete($name);
                }
                $mime = "." . explode("/",$request->file('img_uri')->getClientMimeType())[1];
                $name = date('Ymdhis') . $mime;
                $category->img_uri = '/img/our-cart/' . $name;
                \Storage::disk('our-cart')->put($name, file_get_contents( $request->file('img_uri')->getRealPath() ) );
            }
            $category->save();
        
            return redirect('adm')->with([
                'success' => true
            ]);
        }
        public function deleteCategory(Request $request){
            $category = ProductCategory::find($request->category_id);
            $category->delete();
            return redirect('adm')->with([
                'success' => true
            ]);
        }
        public function categoryForm(Request $request){
            $category = ProductCategory::find($request->id);
            return $category ? view('admin.update.category')->with('category', $category) : redirect('adm');
        }
        public function listProducts(Request $request){
            $category = ProductCategory::find($request->id);
            return $category ? view('admin.sort.products')->with('cat', $category) : redirect('adm');
        }

    /** Restaurant Admin 
     */
        public function handleRestaurant(Request $request){

            $restaurant = $request->restaurant_id ? Restaurant::find($request->restaurant_id) : new Restaurant();
            $restaurant->cost_center = $request->cost_center ? $request->cost_center : $restaurant->cost_center;
            $restaurant->location_name = $request->location_name ? $request->location_name : $restaurant->location_name;
            $restaurant->address = $request->address ? $request->address : $restaurant->address;
            $restaurant->city = $request->city ? $request->city : $restaurant->city;
            $restaurant->lat = $request->lat ? $request->lat : $restaurant->lat;
            $restaurant->lng = $request->lng ? $request->lng : $restaurant->lng;
            $restaurant->schedule = $request->schedule ? $request->schedule : $restaurant->schedule;
            
            $restaurant->wifi_zone = $request->wifi_zone;
            $restaurant->pet_friendly = $request->pet_friendly;
            $restaurant->kid_zone = $request->kid_zone;
            $restaurant->parking = $request->parking;
            $restaurant->valet_parking = $request->valet_parking;
            $restaurant->save();
            
            return redirect('adm')->with([
                'success' => true
            ]);
        }
        public function deleteRestaurant(Request $request){
            $restaurant = Restaurant::find($request->restaurant_id);
            $restaurant->delete();
            return redirect('adm')->with('success',true);
        }
        public function restaurantForm(Request $request){
            $restaurant = Restaurant::find($request->id);

            return $restaurant ? view('admin.update.restaurant')->with('restaurant', $restaurant) : redirect('adm');
        }

    /** News Admin 
     */
        public function handleNew(Request $request){

            $new = $request->new_id ? News::find($request->new_id) : new News();
            $new->title = $request->title ? $request->title : $new->title;
            $new->meta_title = $request->meta_title ? $request->meta_title : $new->meta_title;
            $new->description = $request->description ? $request->description : $new->description;
            $new->button_text = $request->button_text ? $request->button_text : $new->button_text;
            $new->custom_url = $request->custom_url;
            $new->slug = preg_replace("/[^A-Za-z0-9]/", "-", strtolower($new->title));
            
            $message = $request->content_entry;

            $dom = new \DomDocument();
            $dom->loadHtml('<?xml encoding="utf-8" ?>' . $message, 
                LIBXML_HTML_NOIMPLIED | # Make sure no extra BODY
                LIBXML_HTML_NODEFDTD |  # or DOCTYPE is created
                LIBXML_NOERROR |        # Suppress any errors
                LIBXML_NOWARNING        # or warnings about prefixes.
            );
                
    
            $images = $dom->getElementsByTagName('img');
            
            foreach($images as $img){
                $src = $img->getAttribute('src');
                if(preg_match('/data:image/', $src)){
                    preg_match('/data:image\/(?<mime>.*?)\;/', $src, $groups);
                    $mimetype = $groups['mime'];
                    $filename = uniqid();
                    $filepath = "/img/lo-nuevo/$filename.$mimetype";
                    $image = Image::make($src)->encode($mimetype, 100);
                    \Storage::disk('lo-nuevo')->put("$filename.$mimetype", $image );
                    $new_src = asset($filepath);
                    $img->removeAttribute('src');
                    $img->setAttribute('src', $new_src);
                }
            }
    
            $new->content_entry = $dom->saveHTML();

            if($request->file('banner_img')){
                if($request->new_id){
                    $arr = explode("/",$new->banner_img);
                    $name = end($arr);
                    \Storage::disk('sliders')->delete($name);
                }
                $mime = "." . explode("/",$request->file('banner_img')->getClientMimeType())[1];
                $name = date('Ymdhis') . $mime;
                $new->banner_img = '/img/lo-nuevo/' . $name;
                \Storage::disk('lo-nuevo')->put($name, file_get_contents( $request->file('banner_img')->getRealPath() ) );
            }
            if($request->file('campaign_tag_img')){
                if($request->new_id){
                    $arr = explode("/",$new->campaign_tag_img);
                    $name = end($arr);
                    \Storage::disk('sliders')->delete($name);
                }
                $mime = "." . explode("/",$request->file('campaign_tag_img')->getClientMimeType())[1];
                $name = uniqid() . $mime;
                $new->campaign_tag_img = '/img/lo-nuevo/' . $name;
                \Storage::disk('lo-nuevo')->put($name, file_get_contents( $request->file('campaign_tag_img')->getRealPath() ) );
            }
            $new->save();

            return redirect('adm')->with([
                'success' => true
            ]);
        }
        public function deleteNew(Request $request){
            $new = News::find($request->new_id);
            $new->delete();
            return redirect('adm')->with([
                'success' => true
            ]);
        }
        public function newForm(Request $request){
            $new = News::find($request->id);
            return view('admin.update.new')->with([
                'new' => $new
            ]);
        }
/*         public function deleteContent(Request $request){
            $content = Content::find($request->content_id);
            $content->delete();
            return redirect()->back()->with([
                'success' => true
            ]);
        }
        public function addContent(Request $request){
            if ($request->content) {
                $content = $request->content;
                ksort($content);
                foreach ($content as $key => $value) {
                    $con = new Content;
                    //\Log::error(gettype($value));
                    if(gettype($value) == "string"){
                        $con->type = false;
                        preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/", $value, $matches);
                        $con->content = $matches[1];;
                        $con->order = $key;
                    }else{
                        $con->type = true;
                        $mime = "." . explode("/",$value->getClientMimeType())[1];
                        $name = date('Ymdhis') . $mime;
                        $con->content = '/img/lo-nuevo/' . $name;
                        \Storage::disk('lo-nuevo')->put($name, file_get_contents( $value->getRealPath() ) );
                        $con->order = $key;
                    }
                    $con->new_id = $request->new_id;
                    $con->save();
                }
            }
            return redirect()->back()->with([
                'success' => true
            ]);
        }
 */

    public function sortEntity(Request $request){
        $position = $request->position;
        $i=1;
        $entity = (string)"App\\".$request->entity;
        foreach ($position as $key => $value) {
            $e = $entity::find($value);
            $e->order = $i;
            $e->save();
            $i++;
        }
        return response(['success' => true]);
    }

    public function handleBannerRestaurant(Request $request){

        $bannerRestaurant = BannerRestaurant::first();
        $bannerRestaurant->copy = $request->copy;
        
        if($request->file('img_uri')){
            try {
                $arr = explode("/",$bannerRestaurant->img_uri);
                $name = end($arr);
                \Storage::disk('sliders')->delete($name);
            } catch (\Exception $th) {
                //throw $th;
            }
            $arr = explode("/",$bannerRestaurant->img_uri);
            $name = end($arr);
            \Storage::disk('sliders')->delete($name);
            $mime = "." . explode("/",$request->file('img_uri')->getClientMimeType())[1];
            $name = "banner_restaurant" . date('Ymdhis') . $mime;
            $bannerRestaurant->img_uri = '/img/sliders/' . $name;
            \Storage::disk('sliders')->put($name, file_get_contents( $request->file('img_uri')->getRealPath() ) );
        }
        $bannerRestaurant->save();
    
        return redirect('adm')->with([
            'success' => true
        ]);
    }

    /**
     * Legacy code COVID 19
     */
    public function handleContingencia(Request $request){
        if($request->cont_id){
            $cont = Contingencia::find($request->cont_id);
        }else{
            $cont = new Contingencia();
        }
        $cont->city = $cont->city != $request->city ? ucfirst($request->city) : $cont->city;
        $cont->pdv = $cont->pdv != $request->pdv ? $request->pdv : $cont->pdv;
        $cont->address = $cont->address != $request->address ? $request->address : $cont->address;
        $cont->urb = $cont->urb != $request->urb ? $request->urb : $cont->urb;
        $cont->phone = $cont->phone != $request->phone ? $request->phone : $cont->phone;
        $cont->pick_time = $cont->pick_time != $request->pick_time ? $request->pick_time : $cont->pick_time;
        $cont->pick_able = $request->pick_able === "on" ? true : false;
        $cont->rappi = $request->rappi === "on" ? true : false;
        $cont->own_delivery = $request->own_delivery === "on" ? true : false;
        $cont->reopening = $request->reopening === "on" ? true : false;
        $cont->dinning_room = $request->dinning_room === "1" ? true : false;
        $cont->open_time = $cont->open_time != $request->open_time ? $request->open_time : $cont->open_time;
        
        $cont->save();

        return redirect("/adm/contingencia")->with('success', true)->with('contingencia', Contingencia::all());
    }

    public function enableContingencia(Request $request){
       $cont =  Contingencia::find($request->cont_id);
       $cont->active = $cont->active == true ? false : true;
       $cont->save();
       return redirect()->back()->with('success', true)->with('contingencia', Contingencia::all());
    }

    public function deleteContingencia(Request $request){
        Contingencia::find($request->cont_id)->delete();
        return redirect()->back()->with('success', true)->with('contingencia', Contingencia::all());
    }

    public function indexContingencia(){
        return view('admin.covid19')->with('success', true)->with('contingencia', Contingencia::all());
     }
    public function editContingencia(Request $request){
        return view('admin.covid19-edit')->with('success', true)->with('cont', Contingencia::find($request->cont_id));
     }


    /** Legals Admin 
     */
    public function handleLegal(Request $request){

        $legal = $request->legal_id ? Legal::find($request->legal_id) : new Legal();
        $legal->title = $request->title ? $request->title : $legal->title;
        $message = $request->text;
        $dom = new \DomDocument();
        $dom->loadHtml('<?xml encoding="utf-8" ?>' . $message, 
            LIBXML_HTML_NOIMPLIED | # Make sure no extra BODY
            LIBXML_HTML_NODEFDTD |  # or DOCTYPE is created
            LIBXML_NOERROR |        # Suppress any errors
            LIBXML_NOWARNING        # or warnings about prefixes.
        );
        $legal->text = $dom->saveHTML();
        
        $legal->save();
    
        return redirect('adm')->with([
            'success' => true
        ]);
    }
    public function deleteLegal(Request $request){
        $legal = Legal::find($request->legal_id);
        $legal->delete();
        return redirect('adm')->with([
            'success' => true
        ]);
    }
    public function legalForm(Request $request){
        $legal = Legal::find($request->id);
        return $legal ? view('admin.update.legals')->with('legal', $legal) : redirect('adm');
    }

}
