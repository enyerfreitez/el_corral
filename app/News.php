<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    public $table = "news";

    public function contents(){
        return $this->hasMany('App\Content', 'new_id', 'id');
    }
    public function delete(){
        $arr = explode("/",$this->banner_img);
        $name = end($arr);
        \Storage::disk('lo-nuevo')->delete($name);
		parent::delete();
    }
}
