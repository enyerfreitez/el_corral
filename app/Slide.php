<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Slide extends Model
{
    public $timestamps = false;


    /**
    * @see Overrides delete() method to errase all data files related with the Slide
    * @param None
    * @return None
    */
    public function delete(){
        $arr = explode("/",$this->img_uri);
        $name = end($arr);
        \Storage::disk('sliders')->delete($name);
		parent::delete();
    }
}