<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Content extends Model
{

    public $timestamps = false;

    public function delete(){
        if ($this->type) {
            $arr = explode("/",$this->content);
            $name = end($arr);
            \Storage::disk('lo-nuevo')->delete($name);
        }
		parent::delete();
    }
}
