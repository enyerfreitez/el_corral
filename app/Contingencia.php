<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contingencia extends Model
{
    protected $fillable = [
        "city",
        "pdv",
        "address",
        "urb",
        "phone",
        "pick_time",
     ];
    public $table = "contingencia";
    public $timestamps = false;
}
