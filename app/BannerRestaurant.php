<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BannerRestaurant extends Model
{
    public $table = "banner_restaurant";
    public $timestamps = false;
}
