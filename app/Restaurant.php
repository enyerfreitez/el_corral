<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Restaurant extends Model
{
    //SELECT *, ( 6371 * acos( cos( radians(4.7557941) ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(-74.04175130000002) ) + sin( radians(4.7557941) ) * sin( radians( lat ) ) ) ) AS distance FROM restaurants HAVING distance <= 5

    public static function fetchByPosition($lat, $lng, $excluded){
        if($excluded){
            $size = sizeof($excluded);
            $where = "WHERE id NOT IN (";
            for($i = 0; $i < $size; $i++){
                $where = $where . $excluded[$i] . ($i + 1 == $size ? ")" : ", ");
            }
            
            return 
            \DB::select(
                \DB::raw(
                    "SELECT *, ( 6371 * acos( cos( radians(" .$lat. ") ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(".$lng.") ) + sin( radians(".$lat.") ) * sin( radians( lat ) ) ) ) AS distance FROM restaurants " . $where . " HAVING distance <= 5"
                )
            );
        }

        return 
        \DB::select(
            \DB::raw(
                "SELECT *, ( 6371 * acos( cos( radians(" .$lat. ") ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(".$lng.") ) + sin( radians(".$lat.") ) * sin( radians( lat ) ) ) ) AS distance FROM restaurants HAVING distance <= 5"
            )
        );
    }

    public function setWifiZoneAttribute($value){
        $this->attributes['wifi_zone'] = ($value=='on');
    }
    public function setPetFriendlyAttribute($value){
        $this->attributes['pet_friendly'] = ($value=='on');
    }
    public function setParkingAttribute($value){
        $this->attributes['parking'] = ($value=='on');
    }
    public function setKidZoneAttribute($value){
        $this->attributes['kid_zone'] = ($value=='on');
    }
    public function setValetParkingAttribute($value){
        $this->attributes['valet_parking'] = ($value=='on');
    }
}
