<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'LandingController@getHome')->name('home');

Route::group(['prefix' => 'nuestra-carta'], function(){
    Route::get('/', 'LandingController@fetchProductCategories');
    
    Route::get('/{category_slug}', 'LandingController@fetchProducts');
    
});
Route::group(['prefix' => 'sostenibilidad'], function () {
    Route::get('/', function () {
        return view('internal.sostenibilidad.main');
    });
    Route::get('lo-de-afuera-tambien-importa', function () {
        return view('internal.sostenibilidad.empaques');
    });
    Route::get('fundacion-best-buddies', function () {
        return view('internal.sostenibilidad.bestbuddies');
    });
    Route::get('conoce-mas', function () {
        return view('internal.sostenibilidad.conocemas');
    });
});


Route::group(['prefix' => 'lo-nuevo'], function () {
    Route::get('/', 'LandingController@indexNews');
    Route::get('/{slug}', 'LandingController@showNew');
});
Route::get('restaurantes', 'LandingController@getRestaurants');

Route::group(['prefix' => 'eventos-fiestas-y-bonos'], function () {
    Route::get('/', function () {
        return view('internal.side-services.main');
    });
    Route::get('bonos', function () {
        return view('internal.side-services.bonos');
    });
    Route::get('foodtruck-y-eventos', function () {
        return view('internal.side-services.ft');
    });
    Route::get('fiestas', function () {
        return view('internal.side-services.fiestas');
    });
});
Route::get('nosotros', function () {
    return view('internal.about');
});
Route::get('hablanos', function () {
    return view('internal.contact');
});


Route::group(['prefix' =>  'endpoints'], function () {
    Route::get('get-slider-info', 'LandingController@getSliderInfo');
    Route::get('get-restaurants', 'LandingController@fetchRestaurants');
    Route::get('get-contingencia', 'LandingController@fetchPdv');
});

Route::group(['prefix' =>  'adm', 'middleware' => 'auth'], function () {
    Route::get('/', 'AdminController@index');
    Route::group(['prefix' =>  'slides'], function () {
        Route::get('/edit', 'AdminController@slideForm');
        Route::post('/delete', 'AdminController@deleteSlide');
        Route::post('/handle', 'AdminController@handleSlide');
    });
    Route::group(['prefix' =>  'products'], function () {
        Route::get('/edit', 'AdminController@productForm');
        Route::post('/delete', 'AdminController@deleteProduct');
        Route::post('/handle', 'AdminController@handleProduct');
    });
    Route::group(['prefix' =>  'legals'], function () {
        Route::get('/edit', 'AdminController@legalForm');
        Route::post('/delete', 'AdminController@deleteLegal');
        Route::post('/handle', 'AdminController@handleLegal');
    });
    Route::group(['prefix' =>  'categories'], function () {
        Route::get('/edit', 'AdminController@categoryForm');
        Route::get('/list', 'AdminController@listProducts');
        Route::post('/delete', 'AdminController@deleteCategory');
        Route::post('/handle', 'AdminController@handleCategory');//content-delete
    });
    Route::group(['prefix' =>  'restaurants'], function () {
        Route::get('/edit', 'AdminController@restaurantForm');
        Route::post('/delete', 'AdminController@deleteRestaurant');
        Route::post('/handle', 'AdminController@handleRestaurant');
        Route::post('/banner', 'AdminController@handleBannerRestaurant');
    });
    Route::group(['prefix' =>  'news'], function () {
        Route::get('/edit', 'AdminController@newForm');
        Route::post('/delete', 'AdminController@deleteNew');
        Route::post('/handle', 'AdminController@handleNew');
        Route::post('/content-delete', 'AdminController@deleteContent');
        Route::post('/content-add', 'AdminController@addContent');
    });
    Route::get('/sort', 'AdminController@sortEntity');

    Route::group(['prefix' =>  'contingencia'], function () {
        Route::get('/', 'AdminController@indexContingencia');
        Route::get('/edit', 'AdminController@editContingencia');
        Route::post('/handle', 'AdminController@handleContingencia');
        Route::post('/delete', 'AdminController@deleteContingencia');
        Route::get('/enable', 'AdminController@enableContingencia');
    });
});

 // Authentication Routes...
 Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
 Route::post('login', 'Auth\LoginController@login');
 Route::post('logout', 'Auth\LoginController@logout')->name('logout');

 // Password Reset Routes...
 Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
 Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
 Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
 Route::post('password/reset', 'Auth\ResetPasswordController@reset');



 Route::get('/contingencia', function(){
     return redirect('/restaurantes');
 });
 Route::get('/horarios-para-llevar', function(){
     return redirect('/restaurantes');
 });
 Route::get('/horarios-restaurantes', function(){
     return redirect('/restaurantes');
 });
 Route::get('/servicios-restaurantes', function(){
     return redirect('/restaurantes');
 });
 
 Route::get('/terminos-y-condiciones', 'LandingController@legals')->name('terminos-y-condiciones');

 Route::get('/lo-nuevo2', function(){ return view('internal.news.content-new'); })->name('news-v2');
 Route::get('/lo-nuevo3', function(){ return view('internal.news.content-new2'); })->name('news-v3');
 Route::get('/pdf/Autorizacion-de-Tratamiento-de-Datos-Personales.pdf', function(){ 
     return redirect('/pdf/Politica-de-Tratamiento-de-Datos-Personales.pdf'); 
    })->name('data-policy');